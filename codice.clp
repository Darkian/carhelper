(deftemplate domande
	(slot domanda
		(type INTEGER)
	)
	(slot risposta
		(type INTEGER)
	)
)

(deftemplate ritrattazione
	(slot input
		(type SYMBOL)
		(allowed-symbols s n)
	)
)

(deftemplate parte1
	(slot input
		(type SYMBOL)
		(allowed-symbols s n)
	)
)

(deftemplate parte2
	(slot input
		(type SYMBOL)
		(allowed-symbols s n)
	)
)

(deftemplate empty
	(slot auto
		(type SYMBOL)
		(allowed-symbols s n)
	)
	(slot accessori
		(type SYMBOL)
		(allowed-symbols s n)
	)
)

(deftemplate linee_di_accessori
	(slot id
		(type INTEGER)
	)
	(slot nome_linea
		(type STRING)
	)
	(slot accessori
		(type STRING)
	)
	(slot prezzo_totale
		(type FLOAT)
	)
	(slot id_auto
		(type INTEGER)
	)
)

(deftemplate auto
	(slot id
		(type INTEGER)
	)
	(slot marca
		(type SYMBOL)
	)
	(slot modello
		(type STRING)
	)
	(slot motore_nome
		(type STRING)
	)
	(slot motore_alimentazione
		(type SYMBOL)
		(allowed-symbols benzina diesel ibrido full-electric)
	)
	(slot motore_litri
		(type FLOAT)
	)
	(slot motore_kw
		(type INTEGER)
	)
	(slot motore_cilindri
		(type INTEGER)
	)
	(slot motore_cavalli
		(type INTEGER)
	)
	(slot motore_rapporti
		(type INTEGER)
	)
	(slot motore_emissioni
		(type FLOAT)
	)
	(slot motore_carburante_ciclo_urbano
		(type FLOAT)
	)
	(slot motore_carburante_ciclo_extraurbano
		(type FLOAT)
	)
	(slot numero_porte
		(type INTEGER)
	)
	(slot capienza
		(type INTEGER)
	)
	(slot condizioni
		(type SYMBOL)
		(allowed-symbols nuova usata km0)
	)
	(slot tipologia
		(type SYMBOL)
		(allowed-symbols suv berlina citycar compatta sportiva lusso station-wagon cross-over monovolume)
	)
	(slot bagagliaio_litri_min
		(type INTEGER)
	)
	(slot bagagliaio_litri_max
		(type INTEGER)
	)
	(slot anno_uscita
		(type INTEGER)
	)
	(slot prezzo_min
		(type FLOAT)
	)
	(slot prezzo_max
		(type FLOAT)
	)
)

(deffacts fatti
	(linee_di_accessori
		(id 1) (nome_linea "Plus") (accessori "Alzacristalli elettrici anteriori e posteriori, Cerchi in acciaio da 17, Climatizzatore manuale, Consolle centrale con freno di stazionamento manuale,
		Griglia anteriore con inserti argento, Luci diurne, Trazione anteriore, Specchietti retrovisori elettrici in tinta riscaldati con indicatore di direzione, Cambio manuale, Sedile conducente con supporto lombare,
		ECOMode (modalità di guida ecosostenibile), Electric Power-Assisted Steering (EPAS) - servosterzo elettrico, 7 Airbag, Anti-lock Braking System (ABS) - frenata antibloccaggio con Electronic Brakeforce Distribuition (ripartitore elettronico della frenata), 
		Controllo elettronico della stabilita', Emergency Brake Assist (EBA) - assistenza alla frenata di emergenza, Hill Start Assist (assistenza alla partenza in salita), Torque Vectoring Control (controllo della trazione in curva),
		Traction Control System (TCS) - controllo di trazione, Tyre Pressure Monitoring System (sistema monitoraggio pressione pneumatici), Audio CD/MP3 - SYNC con Emergency Assistance - Bluetooth con Voice control - AppLink - USB - 6 speaker, Power Start (pulsante di accensione),
		Fari fendinebbia, Cruise Control con limitatore di velocità, EasyFuel (rifornimento senza tappo)") (prezzo_totale 25500.00) (id_auto 1)
	)
	(linee_di_accessori
		(id 2) (nome_linea "Titanium") (accessori "Alzacristalli elettrici anteriori e posteriori, Cerchi in lega da 17, Trazione anteriore, Cambio manuale, Climatizzatore automatico bi zona, Consolle centrale premium con bracciolo e freno di stazionamento elettrico, 
		Griglia anteriore con inserti argento, Luci diurne LED, Specchietti retrovisori elettrici in tinta riscaldati con indicatore di direzione, Sedile conducente e passeggero con supporto lombare,
		ECOMode (modalità di guida ecosostenibile), Electric Power-Assisted Steering (EPAS) - servosterzo elettrico, 7 Airbag, Anti-lock Braking System (ABS) - frenata antibloccaggio con Electronic Brakeforce Distribuition (ripartitore elettronico della frenata),
		Controllo elettronico della stabilita', Emergency Brake Assist (EBA) - assistenza alla frenata di emergenza, Hill Start Assist (assistenza alla partenza in salita), Torque Vectoring Control (controllo della trazione in curva),
		Curve control (controllo della velocità in curva), Traction Control System (TCS) - controllo di trazione, Tyre Pressure Monitoring System (sistema monitoraggio pressione pneumatici), Audio CD/MP3 - SYNC 3 con Emergency Assistance - Bluetooth con Voice control - Applink - 2 USB - Touchscreen 8 pollici - 6 speaker,
		Power Start (pulsante di accensione), Fari fendinebbia, Cruise Control con limitatore di velocità, EasyFuel (rifornimento senza tappo), Keyless Entry") (prezzo_totale 28250.00) (id_auto 1)
	)
	(linee_di_accessori
		(id 3) (nome_linea "ST-Line") (accessori "Alzacristalli elettrici anteriori e posteriori, Badge ST-Line, Body Styling Kit ST-Line, Cerchi in lega da 18 pollici, Climatizzatore automatico bi zona,
		Consolle centrale premium con bracciolo e freno di stazionamento elettrico, Trazione anteriore, Cambio manuale, Cuciture rosse su volante, cuffia del cambio, bracciolo centrale anteriore, sedili e tappetini, Griglia anteriore con inserti nero gloss, Luci diurne LED,
		Specchietti retrovisori elettrici in tinta riscaldati con indicatore di direzione, Rivestimento Cielo scuro, Sedile conducente e passeggero con supporto lombare, Sospensioni sportive,
		ECOMode (modalità di guida ecosostenibile), Electric Power-Assisted Steering (EPAS) - servosterzo elettrico, 7 Airbag, Parcheggio semi automatico,
		Anti-lock Braking System (ABS) - frenata antibloccaggio con Electronic Brakeforce Distribuition (ripartitore elettronico della frenata), Controllo elettronico della stabilità, Emergency Brake Assist (EBA) - assistenza alla frenata di emergenza,
		Hill Start Assist (assistenza alla partenza in salita), Torque Vectoring Control (controllo della trazione in curva), Curve control (controllo della velocità in curva), Traction Control System (TCS) - controllo di trazione,
		Tyre Pressure Monitoring System (sistema monitoraggio pressione pneumatici), Audio CD/MP3 - SYNC 3 con Emergency Assistance - Bluetooth con Voice control - Applink - 2 USB - Touchscreen 8 pollici - 6 speaker, Power Start (pulsante di accensione),
		Fari fendinebbia, Cruise Control con limitatore di velocità, EasyFuel (rifornimento senza tappo), Sensori di parcheggio anteriori e posteriori") (prezzo_totale 31000.00) (id_auto 1)
	)
	(linee_di_accessori
		(id 4) (nome_linea "Vignale") (accessori "Alzacristalli elettrici anteriori e posteriori, Body Styling Kit Vignale, Cerchi in lega da 18 pollici, Trazione anteriore, Cambio manuale, Climatizzatore automatico bi zona, Consolle centrale premium con bracciolo e freno di stazionamento elettrico,
		Griglia anteriore con design esagonale, Luci diurne anteriori e posteriori LED, Privacy Glass, Specchietti Retrovisori in tinta carrozzeria riscaldati, regolabili e ripiegabili elettricamente con indicatori di direzione e funzione memoria,
		Sedile passeggero con regolazione manuale a 6 vie riscaldato con supporto lombare e funzione memoria, ECOMode (modalità di guida ecosostenibile), Electric Power-Assisted Steering (EPAS) - servosterzo elettrico, 7 Airbag,
		Parcheggio semi automatico, Anti-lock Braking System (ABS) - frenata antibloccaggio con Electronic Brakeforce Distribuition (ripartitore elettronico della frenata), Controllo elettronico della stabilità,
		Emergency Brake Assist (EBA) - assistenza alla frenata di emergenza, Hill Start Assist (assistenza alla partenza in salita), Torque Vectoring Control (controllo della trazione in curva), Curve control (controllo della velocità in curva),
		Traction Control System (TCS) - controllo di trazione, Tyre Pressure Monitoring System (sistema monitoraggio pressione pneumatici), Audio CD/MP3 - SYNC 3 con Emergency Assistance - Bluetooth con Voice control - Applink - 2 USB - Touch Navigation 8 pollici - 9 speaker,
		Power Start (pulsante di accensione), Apertura portabagagli col piede, Fari fendinebbia, Cruise Control con limitatore di velocità, EasyFuel (rifornimento senza tappo), Keyless Entry, Sensori di parcheggio anteriori e posteriori") (prezzo_totale 36400.00) (id_auto 1)
	)
	(auto
		(id 1) (marca Ford) (modello "nuova Kuga") (motore_nome "1.5 Ecoboost") (motore_alimentazione diesel) (motore_litri 1.5) (motore_kw 88) (motore_cilindri 3) (motore_cavalli 150) (motore_rapporti 6) (motore_emissioni 102.0) (motore_carburante_ciclo_urbano 5.4)
		(motore_carburante_ciclo_extraurbano 4.3) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 456) (bagagliaio_litri_max 1653) (anno_uscita 2016) (prezzo_min 25500.0) (prezzo_max 36400.0)
	)
	(linee_di_accessori
		(id 5) (nome_linea "Titanium") (accessori "Cerchi in lega da 19 pollici, Climatizzatore automatico bi zona, Trazione anteriore, Display digitale, Retrovisori esterni riscaldabili con luci di cortesia e ripiegabili elettricamente, Retrovisore interno fotocromatico, Vetri oscurati,
			7 Airbag (include airbag ginocchia conducente), Lane Keeping Aid, Cruise Control (include Automatic Speed Limiting Device), Easy Fuel (rifornimento senza tappo), My Key (chiave personalizzabile), Cambio manuale,
			Power Button (pulsante di accensione), Sistema di riconoscimento dei pedoni, Rear View Camera (telecamera posteriore), SYNC 3 con Touch Navigation (9 speakers con DAB, 2 USB, AUX IN e schermo 8 pollici), Traffic Sign Recognition (riconoscimento automatico segnali stradali), Apertura portabagagli col piede") (prezzo_totale 49900.0) (id_auto 2)
	)
	(linee_di_accessori
		(id 6) (nome_linea "ST-Line") (accessori "Cerchi in lega da 20 pollici Black, Climatizzatore automatico bi zona, Display digitale, Retrovisori esterni riscaldabili con luci di cortesia e ripiegabili elettricamente, Retrovisore interno fotocromatico, Vetri oscurati, Griglia inferiore frontale con design unique,
			Sospensioni sportive, Pedaliera in alluminio, 7 Airbag (include airbag ginocchia conducente), Lane Keeping Aid, Cruise Control (include Automatic Speed Limiting Device), Easy Fuel (rifornimento senza tappo), My Key (chiave personalizzabile),
			Power Button (pulsante di accensione), Sistema di riconoscimento dei pedoni, Rear View Camera (telecamera posteriore), SYNC 3 con Sony Touch Navigation (12 speakers con DAB, subwoofer, 2 USB, AUX IN e schermo 8 pollici),
			Traffic Sign Recognition (riconoscimento automatico segnali stradali), Apertura portabagagli col piede") (prezzo_totale 51400.0) (id_auto 2)
	)
	(linee_di_accessori
		(id 7) (nome_linea "Vignale") (accessori "Cerchi in lega da 20 pollici Unique, Climatizzatore automatico bi zona, Trazione anteriore, Cambio manuale, Display digitale, Retrovisori esterni riscaldabili con luci di cortesia e ripiegabili elettricamente, Retrovisore interno fotocromatico, Vetri oscurati,
			Griglia inferiore frontale con design unique, Sospensioni sportive, Pedaliera in alluminio, Griglia frontale superiore con design esagonale, 7 Airbag (include airbag ginocchia conducente), Lane Keeping Aid, Dynamic LED Headlights (fari adattivi intelligenti LED),
			Cruise Control (include Automatic Speed Limiting Device), Easy Fuel (rifornimento senza tappo), My Key (chiave personalizzabile), Power Button (pulsante di accensione), Sistema di riconoscimento dei pedoni,
			Telecamere, Adaptive Steering Wheel (volante con sterzo adattivo), SYNC 3 con Sony Touch Navigation (12 speakers con DAB, subwoofer, 2 USB, AUX IN e schermo 8 pollici), Traffic Sign Recognition (riconoscimento automatico segnali stradali), Apertura portabagagli col piede") (prezzo_totale 55650.0) (id_auto 2)
	)
	(auto
		(id 2) (marca Ford) (modello "Edge") (motore_nome "TDCi") (motore_alimentazione diesel) (motore_litri 2.0) (motore_cilindri 3) (motore_cavalli 180) (motore_rapporti 6) (motore_emissioni 149.0) (motore_carburante_ciclo_urbano 6.5)
		(motore_carburante_ciclo_extraurbano 4.5) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 600) (bagagliaio_litri_max 800) (anno_uscita 2017) (prezzo_min 49900.0) (prezzo_max 55650.0)
	)
	(linee_di_accessori
		(id 8) (nome_linea "Plus") (accessori "Cerchi in acciaio da 16 pollici, Ford Flat System – sedili posteriori abbattibili 60/40, Fari a LED diurni, 7 Airbag, Allarme perimetrale, Anti-lock Braking System – ABS (frenata antibloccaggio), EBD (ripartitore elettronico della frenata), Emergency Brake Assist – EBA (assistenza alla frenata d’emergenza),
			Controllo elettronico della stabilita') + TCS (controllo elettronico della trazione), KIT riparazione pneumatici, Pressure Monitoring System (monitoraggio della pressione degli pneumatici), Climatizzatore a controllo manuale, MyKey, Radio schermo 4,2 pollici TFT con 2 prese USB e 6 speakers, Trazione anteriore, Cambio manuale") (prezzo_totale 19900.0) (id_auto 3)
	)
	(linee_di_accessori
		(id 9) (nome_linea "Titanium") (accessori "Cerchi in lega da 16 pollici, Tergicristalli con sensore pioggia, Barre alluminium look longitudinali sul tetto, Interni in pelle parziale, 7 Airbag, Allarme perimetrale, Anti-lock Braking System – ABS (frenata antibloccaggio), EBD (ripartitore elettronico della frenata),
			Emergency Brake Assist – EBA (assistenza alla frenata d’emergenza), Controllo elettronico della stabilita' + TCS (controllo elettronico della trazione), KIT riparazione pneumatici, Pressure Monitoring System (monitoraggio della pressione degli pneumatici), Fari fendinebbia anteriori con dettagli cromati, MyKey,
			Computer di bordo con schermo da 4,2 pollici a colori, Cruise Control, Climatizzatore automatico bi zona, Keyless Start, SYNC 3 con radio touchscreen 6,5 pollici e 6 speakers, Trazione anteriore, Cambio manuale") (prezzo_totale 23150.0) (id_auto 3)
	)
	(linee_di_accessori
		(id 10) (nome_linea "ST-Line") (accessori "Cerchi in lega da 17 pollici bruniti ST-Line, Tergicristalli con sensore pioggia, Retrovisori elettrici ripiegabili e riscaldati, in tinta carrozzeria e con indicatori di direzione integrati, Barre Black longitudinali sul tetto, Sospensioni sportive, Tetto a contrasto, Interni in pelle parziale, 7 Airbag,
			Allarme perimetrale, Anti-lock Braking System – ABS (frenata antibloccaggio), EBD (ripartitore elettronico della frenata), Emergency Brake Assist – EBA (assistenza alla frenata d’emergenza), Controllo elettronico della stabilita' + TCS (controllo elettronico della trazione), KIT riparazione pneumatici, Pressure Monitoring System (monitoraggio della pressione degli pneumatici),
			Fari fendinebbia anteriori con dettagli cromati, MyKey, Computer di bordo con schermo da 4,2 pollici a colori, Cruise Control, Climatizzatore automatico bi zona, Keyless Start, Keyless Entry, Sensori di parcheggio posteriori, SYNC 3 con radio touchscreen NAV8 pollici e 7 speakers, Trazione anteriore, cambio manuale") (prezzo_totale 24700.0) (id_auto 3)
	)
	(linee_di_accessori
		(id 11) (nome_linea "ST-Line Black Edition") (accessori "Cerchi in lega da 17 pollici bruniti ST-Line, Tergicristalli con sensore pioggia, Retrovisori elettrici ripiegabili e riscaldati, in tinta carrozzeria e con indicatori di direzione integrati, Barre Black longitudinali sul tetto, Sospensioni sportive, Tetto a contrasto Tiger, Interni in pelle parziale,
			7 Airbag, Allarme perimetrale, Anti-lock Braking System – ABS (frenata antibloccaggio), EBD (ripartitore elettronico della frenata), Emergency Brake Assist – EBA (assistenza alla frenata d’emergenza), Controllo elettronico della stabilita' + TCS (controllo elettronico della trazione), KIT riparazione pneumatici, Pressure Monitoring System (monitoraggio della pressione degli pneumatici),
			Fari fendinebbia anteriori con dettagli cromati, MyKey, Computer di bordo con schermo da 4,2 pollici a colori, Cruise Control, Climatizzatore automatico bi zona, Keyless Start, Keyless Entry, Sensori di parcheggio posteriori, SYNC 3 con radio touchscreen NAV8 pollici e 7 speakers, Trazione anteriore, Cambio manuale") (prezzo_totale 25400.0) (id_auto 3)
	)
	(auto
		(id 3) (marca Ford) (modello "Kuga Ecosport") (motore_nome "TDCi") (motore_alimentazione diesel) (motore_litri 1.5) (motore_kw 73) (motore_cilindri 4) (motore_cavalli 100) (motore_rapporti 6) (motore_emissioni 107.0) (motore_carburante_ciclo_urbano 4.5)
			(motore_carburante_ciclo_extraurbano 3.8) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 355) (bagagliaio_litri_max 1238) (anno_uscita 2016) (prezzo_min 19900.0) (prezzo_max 25400.0)
	)
	(linee_di_accessori
		(id 12) (nome_linea "Style") (accessori "2 luci di lettura LED anteriori e 2 posteriori, 2 slot USB, anche per iPod/iPhone, 6 altoparlanti, Adaptive Cruise Control - ACC, Adaptive Cruise Control e Front Assist, Alzacristalli elettrici, App-Connect, Sistema di mantenimento della carreggiata,
			Attivitazione automatica luce di marcia, con luci diurne, funzione Leaving e Coming Home manuale, Bracciolo centrale anteriore, Bulloni antifurto con sistema antifurto ampliato, Cassetto sotto il sedile anteriore destro, Cerchi in lega leggera Mayfield 7 J x 17, Trazione anteriore, Cambio manuale,
			Chiusura centralizzata senza dispositivo di sicurezza, con telecomandoe 2 chiavi estraibili a radiofrequenze, Climatizzatore manuale, Design Pack, Display multifunzione Plus, Fari fendinebbia e luce direzionale, Sistema di riconoscimento della fatica, Front Assist - Sistema di assistenza alla frenata,
			Sensori di parcheggio anteriori e posteriori, Radio Composition Media, Sistema di riconoscimento pedoni, Rivestimento ai sedili in tessuto, motivo Tracks 4, Rivestimento porte e pannelli laterali, Sensore pioggia, Sistema start-stop con sistema di recupero energia in frenata, Telecamera multifunzionale, Tire Mobility Set, Volante multifunzione in pelle") (prezzo_totale 30400.0) (id_auto 4)
	)
	(linee_di_accessori
		(id 13) (nome_linea "Advanced") (accessori "2 luci di lettura LED anteriori e 2 posteriori, 2 slot USB, anche per iPod/iPhone, 6 altoparlanti, Active Info Display, Adaptive Cruise Control - ACC, Adaptive Cruise Control e Front Assist, Alzacristalli elettrici, App-Connect, Sistema di mantenimento della carreggiata, Attivitazione automatica luce di marcia, con luci diurne, funzione Leaving e Coming Home manuale,
			Bracciolo centrale anteriore, Cerchi in lega Grange Hill 7 J x 18 pollici, verniciatura in argento sterling, con pneumatici 215/50 R18, Chiusura centralizzata senza dispositivo di sicurezza, con telecomando e 2 chiavi estraibili a radiofrequenze, Climatizzatore automatico bizona Air Care Climatronic con filtro anti allergeni, Fari anteriori full LED, con luci diurne trapezoidali e indicatori di direzione integrati,
			Fari fendinebbia e luce direzionale, Fari LED per luce anabbagliante e abbagliante, Sistema di riconoscimento della fatica, Front Assist - Sistema di assistenza alla frenata, Interfaccia telefono, Pacchetto Luce E Visibilita', Sensori di parcheggio anteriori e posteriori, Radio Composition Media, Regolazione dinamica della profondità dei fari, Trazione anteriore,
			Riconoscimento pedoni, Rivestimento sedili in tessuto motivo Gem, Sedili anteriori sportivi, Sensore pioggia, Sistema start-stop con sistema di recupero energia in frenata, Spia controllo pneumatici, Telecamera multifunzionale, Tire Mobility Set, Volante multifunzione in pelle, Cambio manuale") (prezzo_totale 32400.0) (id_auto 5)
	)
	(linee_di_accessori
		(id 14) (nome_linea "T-Roc") (accessori "2 luci di lettura LED anteriori e 2 posteriori, 2 slot USB, anche per iPod/iPhone, 6 altoparlanti, Active Info Display, Adaptive Cruise Control - ACC, Adaptive Cruise Control e Front Assist, Alzacristalli elettrici, App-Connect, Sistema di mantenimento della carreggiata, Attivitazione automatica luce di marcia, con luci diurne, funzione Leaving e Coming Home manuale,
			Bracciolo centrale anteriore, Cerchi in lega Grange Hill 7 J x 18 pollici, verniciatura in argento sterling, con pneumatici 215/50 R18, Chiusura centralizzata senza dispositivo di sicurezza, con telecomando e 2 chiavi estraibili a radiofrequenze, Climatizzatore automatico bi zona Air Care Climatronic con filtro anti allergeni, Fari anteriori full LED, con luci diurne trapezoidali e indicatori di direzione integrati,
			Fari fendinebbia e luce direzionale, Fari LED per luce anabbagliante e abbagliante, Sistema di riconoscimento della fatica, Front Assist - Sistema di assistenza alla frenata, Interfaccia telefono, Pacchetto Luce E Visibilita', Sensori di parcheggio anteriori e posteriori, Radio Composition Media, Regolazione dinamica della profondità dei fari,
			Sistema di riconoscimento pedoni, Rivestimento sedili in tessuto motivo Gem, Sedili anteriori sportivi, Sensore pioggia, Sistema start-stop con sistema di recupero energia in frenata, Spia controllo pneumatici, Telecamera multifunzionale, Tire Mobility Set, Volante multifunzione in pelle, Trazione anteriore, Cambio automatico DSG") (prezzo_totale 34400.0) (id_auto 6)
	)
	(auto
		(id 5) (marca Wolfswagen) (modello "T-Roc") (motore_nome "TDi BlueMotion Technology 4MOTION") (motore_alimentazione diesel) (motore_litri 2.0) (motore_kw 110) (motore_cilindri 4) (motore_cavalli 150) (motore_rapporti 7) (motore_emissioni 132.0) (motore_carburante_ciclo_urbano 6.0)
			(motore_carburante_ciclo_extraurbano 4.6) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 392) (bagagliaio_litri_max 1297) (anno_uscita 2017) (prezzo_min 32400.0) (prezzo_max 32400.0)
	)
	(auto
		(id 6) (marca Wolfswagen) (modello "T-Roc") (motore_nome "TDi BlueMotion Technology 4MOTION") (motore_alimentazione diesel) (motore_litri 2.0) (motore_kw 110) (motore_cilindri 4) (motore_cavalli 150) (motore_rapporti 7) (motore_emissioni 135.0) (motore_carburante_ciclo_urbano 5.7)
			(motore_carburante_ciclo_extraurbano 4.8) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 342) (bagagliaio_litri_max 1297) (anno_uscita 2017) (prezzo_min 34400.0) (prezzo_max 34400.0)
	)
	(linee_di_accessori
		(id 15) (nome_linea "Urban") (accessori "8 altoparlanti, App-Connect, Attivazione automatica luce di marcia, con luci diurne, funzione leaving e funzione coming home manuale, Climatizzatore manuale, Display multifunzione Plus, Interfaccia telefono, Sistema di mantenimento della carreggiata, Pacchetto tecnico, Radio Composition Media, Sensore pioggia, Sistema start-stop con sistema di recupero energia in frenata, Spia controllo pneumatici,
			Tire Mobility Set, Volante multifunzione in pelle, Cambio manuale, Trazione anteriore, Cerchi in lega") (prezzo_totale 27200.0) (id_auto 7)
	)
	(auto
		(id 7) (marca Wolfswagen) (modello "nuova Tiguan") (motore_nome "TDI SCR - Diesel Common Rail") (motore_alimentazione diesel) (motore_litri 1.6) (motore_kw 85) (motore_cilindri 4) (motore_cavalli 115) (motore_rapporti 6) (motore_emissioni 125.0) (motore_carburante_ciclo_urbano 5.8)
			(motore_carburante_ciclo_extraurbano 4.2) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 615) (bagagliaio_litri_max 1655) (anno_uscita 2017) (prezzo_min 27200.0) (prezzo_max 27200.0)
	)
	(linee_di_accessori
		(id 16) (nome_linea "Urban") (accessori "8 altoparlanti, App-Connect, Attivazione automatica luce di marcia, con luci diurne, funzione leaving e funzione coming home manuale, Climatizzatore manuale, Display multifunzione Plus, Interfaccia telefono, Sistema di mantenimento della carreggiata, Pacchetto tecnico, Radio Composition Media, Sensore pioggia, Sistema start-stop con sistema di recupero energia in frenata, Spia controllo pneumatici,
			Tire Mobility Set, Volante multifunzione in pelle, Cambio manuale, Trazione integrale 4MOTION, Cerchi in lega") (prezzo_totale 27200.0) (id_auto 8)
	)
	(auto
		(id 8) (marca Wolfswagen) (modello "nuova Tiguan") (motore_nome "TDI SCR - Diesel Common Rail") (motore_alimentazione diesel) (motore_litri 1.6) (motore_kw 85) (motore_cilindri 4) (motore_cavalli 115) (motore_rapporti 6) (motore_emissioni 125.0) (motore_carburante_ciclo_urbano 5.8)
			(motore_carburante_ciclo_extraurbano 4.2) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 615) (bagagliaio_litri_max 1655) (anno_uscita 2017) (prezzo_min 27200.0) (prezzo_max 27200.0)
	)
	(linee_di_accessori
		(id 17) (nome_linea "Style") (accessori "8 altoparlanti, Adaptive Cruise Control - ACC, App-Connect, Attivazione automatica luce di marcia, con luci diurne, funzione leaving e funzione coming home manuale, Cerchi in lega leggera Montana 7 J x 17, Climatizzatore manuale e vetro termico, Display multifunzione Premium con display a colori, Fari fendinebbia e luce direzionale, Front Assist - Sistema di assistenza alla frenata, Interfaccia telefono,
			Sistema di mantenimento della carreggiata, Parcheggio semi automatico, Pacchetto tecnico, Radio Composition Media, Rivestimento sedili in stoffa, motivo Rhombus, Sedili anteriori comfort, Sensore pioggia, Sistema start-stop con sistema di recupero energia in frenata, Spia controllo pneumatici, Tavolini ribaltabili agli schienali dei sedili anteriori, Tire Mobility Set, Volante multifunzione in pelle, Cambio automatico, Trazione Integrale") (prezzo_totale 37500.0) (id_auto 9)
	)
	(auto
		(id 9) (marca Wolfswagen) (modello "nuova Tiguan") (motore_nome "TDI SRC 4MOTION - Diesel Common Rail") (motore_alimentazione diesel) (motore_litri 2.0) (motore_kw 110) (motore_cilindri 4) (motore_cavalli 150) (motore_rapporti 7) (motore_emissioni 147.0) (motore_carburante_ciclo_urbano 6.7)
			(motore_carburante_ciclo_extraurbano 5.0) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 975) (bagagliaio_litri_max 1746) (anno_uscita 2017) (prezzo_min 37500.0) (prezzo_max 37500.0)
	)
	(linee_di_accessori
		(id 18) (nome_linea "Ambition") (accessori "Cerchi in lega leggera Triton 7,0J da 17 pollici, Trazione anteriore, Cambio manuale, Comando cromato interno delle lamelle delle prese di aria, Griglia con cornice e listelli cromati, Interni in tessuto nero o rosso con inserti decorativi Cool Brushed/Graphite Metalli, Luci posteriori a LED, Mancorrenti al tetto di colore nero, Maniglie interne delle portiere cromate, Paraurti anteriori e posteriore in colore carrozzeria, Regolatore manuale profondità fari cromato,
			Rivestimento passaruota in colore nero (solo motori 4x4), Specchietti retrovisori esterni e maniglie delle portiere in colore carrozzeria, Terminale di scarico nascosto, 3 poggiatesta posteriori e 2 anteriori regolabili in altezza, Airbag laterali anteriori e Airbag per la testa a tendina, Airbag lato conducente, Airbag lato passaggero con dispositivo di disattivazione airbag, Airbag per le ginocchia lato conducente, Attivazione luci emergenza e sblocco automatico chiusura centralizzata in caso di incidente,
			Ausilio per partenza in salita, Catarifrangenti di sicurezza sulle portiere anteriori, Cinture di sicurezza anteriori a 3 punti regolabili in altezza con pre-tensionatori, Dispositivo antiavviamento elettronico (immobilizer), numero di telaio visibile sul parabrezza, Sistema di riconoscimento della fatica, Fari fendinebbia anteriori, Fari Fendinebbia posteriore a LED, Sistema di riconoscimento pedoni - monitoraggio radar dello spazio antistante la vettura e funzione di frenata automatica di emergenza,
			Interruzione alimentazione carburante in caso d’incidente, Luce di stop a LED in spoiler sul tetto, Sistema di ancoraggio Isofi x per 2 seggiolini per bambini nei sedili posteriori, Sistema di protezione contro errato rifornimento carburante, Spia allacciamento cinture di sicurezza e avviso acustico per tutti i passeggeri, Spia monitoraggio pressione pneumatici (TPM), Tre cinture di sicurezza posteriori a tre punti, Triangolo di emergenza e kit primo soccorso, 4 maniglie al tetto pieghevoli con ganci, 8 altoparlanti, Alette parasole con specchietti di cortesia illuminati,
			Alzacristalli elettrici anteriori e posteriori con chiusura sicurezza per bambini, Antenna per la banda FM, Chiusura centralizzata con telecomando (2 chiavi pieghevoli), Climatizzatore automatico bi zona con AQS (Air Quality Sensor), Luci di lettura centrali anteriori, Sensori di parcheggio posteriori con funzione frenante, Radio Bolero con display da 8 pollici, Bluetooth®, presa USB con connettività Apple, comandi vocali, slot per scheda SD, Regolatore di velocità (Cruise Contol con limitatore di velocita'),
			Regolazione manuale supporto lombare sedili anteriori, Sedili anteriori regolabili in altezza, Sedili frazionabili 60/40 con possibilità di reclinare gli schienali e le sedute, Set di reti per bagagliaio, SMARTLINK+ - Include CarPlay (Apple), Android Auto (Google), MirrorLink e SMARTGATE, Apertura portabagagli col piede, Specchietti retrovisori esterni laterali regolabili, riscaldabili e ripiegabili elettricamente e schermabili automaticamente con luci di ingombro e indicatori di direzione integrati,
			Specchietto retrovisore interno schermabile automaticamente, Torcia LED rimovibile nel vano bagagli, Bracciolo centrale anteriore con vano Jumbo Box refrigerato e presa da 12V, Consolle centrale con portabicchiere aperto, sistema Easy Open, Display multifunzione con computer di bordo Maxi DOT con funzione suggerimento cambio marcia, Fari alogeni regolabili manualmente in altezza e luci diurne a LED, Funzione Start & Stop e recupero dell'energia in frenata") (prezzo_totale 26100.0) (id_auto 10)
	)
	(auto
		(id 10) (marca Skoda) (modello "Karoq") (motore_nome "TDI") (motore_alimentazione diesel) (motore_litri 1.6) (motore_kw 85) (motore_cilindri 4) (motore_cavalli 115) (motore_rapporti 6) (motore_emissioni 120.0) (motore_carburante_ciclo_urbano 5.0)
			(motore_carburante_ciclo_extraurbano 4.4) (numero_porte 5) (capienza 5) (condizioni nuova) (tipologia suv) (bagagliaio_litri_min 521) (bagagliaio_litri_max 1630) (anno_uscita 2017) (prezzo_min 26100.0) (prezzo_max 26100.0)
	)
)

(defrule inizio_random
	(declare (salience 1))
	=>
	(while (neq (get-strategy) random)
		(set-strategy random)
	)
	(printout t "*****	SISTEMA ESPERTO DI SUPPORTO ALL'ACQUISTO DI UNA AUTOMOBILE	*****"crlf)
	(printout t "Sviluppato da Luigi Manosperta, matricola 652735, corso di Ingegneria della Conoscenza e Sistemi Esperti, a.a. 2017/2018, C.d.L. Informatica, Universita' degli studi di Bari 'Aldo Moro'	
		
		
	"crlf)
	(set-salience-evaluation every-cycle)
	(assert (parte1 (input s)))
)

(defrule domanda1
	(parte1 (input s))
	=>
	(printout t "A quale fascia di potenza e' interessato?" crlf)
	(printout t "1- Tra gli 80 e i 100cv" crlf)
	(printout t "2- Tra i 100 e i 120cv" crlf)
	(printout t "3- Oltre i 120cv" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Questo dipende molto dai suoi interessi, meglio da cio' che ne deve fare dell'auto. Se la volesse perche' effettivamente fa percorsi fuori-strada
					o comunque la dovrebbe usare molto spesso su tracciati non asfaltati, allora non sarebbe una cattiva idea ragionare sulla fascia 2. Al contrario, puo' optare
					per la fascia 1. Infine, se per l'uso che ne dovra' fare dovesse ritenere che viaggera' sempre con 4-5 persone a bordo (ipotizzando che 4-5 sia la capienza massima),
					e magari anche con il bagagliaio pieno, allora le consiglio vivamente la fascia 3.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Le e' stata fatta questa domanda perche' ha comunicato di essere interessato a veicoli per uso extraurbano, specialmente ad un suv.
					Anche questa e' una domanda fondamentale in quanto e' impensabile scegliere un'auto senza specificare quanto potente la si vuole,
					e di questa scelta ovviamente il sistema esperto ne terra' conto per proporle l'auto che piu' si avvicina alle sue aspettative.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(default none)
		)
	)
	(assert (domande (domanda 1) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda2
	(parte1 (input s))
	=>
	(printout t "Qual e' la sua fascia di eta'?" crlf)
	(printout t "1- tra i 18 e i 30 anni" crlf)
	(printout t "2- tra i 31 e i 40 anni" crlf)
	(printout t "3- tra i 41 e i 60 anni" crlf)
	(printout t "4- oltre i 60 anni" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 5) (< ?r 1) (> ?r 5))
		(printout t "Le ho fatto questa domanda perche' le necessita' che ha un giovane magari potrebbero non corrispondere
			a quelle di una persona anziana; il che significa che se ha la sia eta' corrisponde al caso 1 significa che potrebbe
			essere interessato ad accessori diversi rispetto a quelli scelti da persone piu' avanti con gli anni.
			Cosa vuole scegliere allora?" crlf)
		(bind ?r (read))
	)
	(assert (domande (domanda 2) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda3
	(parte1 (input s))
	=>
	(printout t "Vorrebbe l'auto nuova?" crlf)
	(printout t "1- Si, la prendo nuova" crlf)
	(printout t "2- No, la prendo usata" crlf)
	(printout t "3- No, la prendo km 0" crlf)
	(printout t "4- Indifferente" crlf)
	(printout t "5- Help" crlf)
	(printout t "6- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 5) (eq ?r 6) (< ?r 1) (> ?r 6))
		(switch ?r
			(case 5 then
				(printout t "Il fatto di volere un'auto nuova significa che vuole garanzia che i componenti siano di fabbrica, che non
					sia mai stata usata da nessun altro, e soprattutto che sia personalizzabile nel senso che puo' selezionare gli
					accessori che desidera in maniera completamente libera. D'antro canto, se dovesse scegliere un'auto usata sicuramente
					avra' qualche segno di usura a livello estetico cosi' come nella parte meccanica dell'auto, e comunque sebbene il prezzo
					scenda di molto non puo' aggiungere alcun tipo di accessorio, neanche post-vendita. Ultimo caso e' quello dell'auto
					a km 0, il che significa che non ha mai avuto un proprietario e che ha un prezzo comunque leggermente piu' basso di
					quello di listino, ma e' in piu' che buone condizioni sia a livello estetico che meccanico, quindi di prestazioni.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 6 then
				(printout t "Questa domanda sostanzialmente comunica al sistema cosa lei cerca a livello di disponibilita' di pezzi di auto,
					dato che se dovesse scegliere un'auto usata godrebbe di alcuni privilegi ma ne sacrificherebbe altri.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 3) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda4
	(parte1 (input s))
	=>
	(printout t "L'auto le serve per spostarsi in citta' o fuori citta', usualmente?" crlf)
	(printout t "1- Per spostarmi in citta'" crlf)
	(printout t "2- Per spostarmi fuori citta'" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "E' necessario per il sistema capire l'uso dell'auto dato che vuole scegliere dato che questo implica
					che se sceglie un'auto per uso extraurbano non le viene proposta una citycar; al contrario, se sceglie un'auto per uso
					urbano non le verra' proposta una sportiva o un suv.
					Cosa sceglie quindi?" clrf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Questa e' una domanda fondamentale, perche' il sistema sara' in grado di supportarla nella scelta di auto che saranno
					solo extraurbano, in particolare solo suv, anziche' urbano -citycar e al piu' monovolume -.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 4) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda5
	(parte1 (input s))
	=>
	(printout t "Che tipo di alimentazione preferirebbe per l'auto?" crlf)
	(printout t "1- Diesel" crlf)
	(printout t "2- Ibrida" crlf)
	(printout t "3- Full-electric" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Un'auto e' ibrida se ha due modalita' di funzionamento: gas/combustibile tradizionale + elettrico.
					Quindi, sostanzialmente, il guidatore pua' decidere in qualsiasi momento se guidare con consumo di
					gas/combustibile tradizionale oppure continuare la corsa sfruttando la corrente elettrica del secondo motore.
					Un'auto e' full-electric se la sua unica propulsione e' l'elettricita', e non altri mix di energie.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Anche questa e' una domanda fondamentale dato che l'alimentazione e' la caratteristica principale che
					permette di distinguere le auto tra di loro, anche in termini economici oltre che prestazionali. Inoltre, questo
					impatta anche sul tipo di rifornimento che fara' e sui relativi costi: se la sceglie ibrida un buon 50% dei costi di
					rifornimento presso distributori di gasolio o altri comnustibili è azzerato; con l'elettrico più del 50%.
					Cosa seglie quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 5) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda6
	(domande (domanda 4) (risposta 2))
	(parte1 (input s))
	=>
	(printout t "Che tipo di auto vorrebbe?
		1- Berlina
		2- SUV
		3- Sportiva
		4- Station-wagon
		5- Compatta
		6- Lusso
		7- Help
		8- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 7) (eq ?r 8) (< ?r 1) (> ?r 8))
		(switch ?r
			(case 7 then
				(printout t "Un'auto e' berlina se ha 5 porte e ha un assetto generalmente medio-basso con potenza alta e molto rifinita, e
					generalmente il bagagliaio non e' molto spazioso proprio per ottimizzare i consumi;	e' invece SUV se e' pensata per uso su sterrato
					e ha dimensioni generalmente grandi, almeno 3.5m di lunghezza e bagagliaio relativamente ampio, con altissima potenza ed emissioni e consumi;
					e' sportiva quando e' generalmente 3 porte, con un bagagliaio piccolo per favorire l'incredibile potenza, a scapito dello stesso bagagliaio,
					della comodita' di guida e dei consumi; e' station-wagon se ha 5 porte, molto allungata, con assetto basso e con un abitacolo abbastanza spazioso,
					con un bagagliaio generalmente ampio e capiente; e' compatta se ha generalmente 5 porte, l'abitacolo e' abbastanza comodo e ha un bagagliaio dalle
					dimensioni non eccessive ma neanche insignificanti, una versione ridotta della berlina; e' di lusso quando l'accessoristica, la potenza e il comfort
					di guida sono decisamente il top delle soluzioni di mercato, nonche' una garanzia furto e incendio durevole e assistenza 24h/7g.
					Cosa sceglie quindi?" crlf)
				(bind ?r (read))
			)
			(case 8 then
				(printout t "Questa e' una domanda che guarda ai gusti personali dell'utente, in modo da indirizzare la scelta sul tipo di auto che piu' le
					interessa, senza che il sistema le proponga auto che non le piacciano.
					Cosa sceglie quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 6) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda7
	(parte1 (input s))
	=>
	(printout t "Ha qualche preferenza in termini di case automobilistiche?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Questa domanda e' di grande importanza se dovesse ritenere di preferire una certa marca ad un'altra, cosi' il sistema le proporra' un modello di auto
					della marca che preferisce. Se tuttavia dovesse non avere preferenze, il sistema la guidera' verso la soluzione piu' vicina ai suoi interessi.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "In realta' questa domanda serve al sistema per capire se consigliarle una marca di auto di suo gradimento oppure muoversi su altri tipi di auto, semplicemente.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 7) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda8
	(domande (domanda 7) (risposta 1))
	(parte1 (input s))
	=>
	(printout t "Indichi quale tra queste preferisce maggiormente (UNA sola risposta ammessa):" crlf)
	(printout t "1- Ford" crlf)
	(printout t "2- Mazda" crlf)
	(printout t "3- Opel" crlf)
	(printout t "4- Wolfswagen" crlf)
	(printout t "5- Nissan" crlf)
	(printout t "6- Fiat" crlf)
	(printout t "7- Skoda" crlf)
	(printout t "8- Jaguar" crlf)
	(printout t "9- Maserati" crlf)
	(printout t "10- Audi" crlf)
	(printout t "11- Mercedes-Benz" crlf)
	(printout t "12- BMW" crlf)
	(printout t "13- Chevrolet" crlf)
	(printout t "14- Renault" crlf)
	(printout t "15- Smart" crlf)
	(printout t "16- Dacia" crlf)
	(printout t "17- Sukuzi" crlf)
	(printout t "18- Perche mi fai questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 18) (< ?r 1) (> ?r 18))
		(printout t "Il sistema ha appreso, tramite la domanda che le chiedeva le sue preferenze in termini di case automobilistiche, che effettivamente
			lei ha una marca a cui e' molto affezionato: il sistema pertanto le chiede quale di queste marche e' quella che lei preferisce.
			Quale preferisce quindi?" crlf)
		(bind ?r (read))
	)
	(assert (domande (domanda 8) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda9
	(parte1 (input s))
	=>
	(printout t "Vuole che il bagagliaio sia capiente?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- Non necessariamente" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Un bagagliaio ampio significa che ha piu' spazio per riporre valige e oggetti di vario tipo, ma sostanzialmente questo,
					a bagagliaio vuoto, significa che toglie spazio all'abitacolo a meno che i sedili posteriori non siano abbattibili. In generale,
					a prescindere se i sedili posteri siano abbattibili, se dovesse scegliere un bagagliaio ampio e dovesse riempirlo per la quasi
					totalita' dei litri possibili, questo impatterebbe sulle prestazioni, cioe' una velocita' di crociera piu' ridotta, quindi se
					vuole andare piu' veloce consumera' di piu' e, implicitamente, quest'ultimo aspetto si trasforma in piu' rifornimenti.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Anche se potrebbe non sembrare, questa domanda ha senso dato che lei magari preferisce un'auto si' potente ma anche con
					un bagagliaio abbastanza ampio, oppure semplicemente cerca un'auto compatta con dimensioni di portabagagli non insignificanti. Questo
					ovviamente fa la differenza quando il sistema esperto dovra' alla fine suggerire l'auto che piu' si adatta alle sue necessita'.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 9) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda10-d3r1
	(domande (domanda 3) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(parte1 (input s))
	=>
	(printout t "Vuole un'auto che e' presente da poco sul mercato o l'inverso?" crlf)
	(printout t "1- No, una che e' sul mercato da circa 5 anni" crlf)
	(printout t "2- No, una che e' sul mercato da circa 10 anni" crlf)
	(printout t "3- Si, una che e' sul mercato da un paio di anni" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Con questa domanda il sistema vuole capire anche quanto e' disposto a spendere, nonche' capire quali sono i pregi delle auto che sono state lanciate in quei periodi sopra indicati,
					dato che questo significa piu' optional, piu' potenza, piu' affidabilita', nonche' una gestione del consumo del carburante migliore. Infatti, dato che la cerca nuova,
					non sarebbe una cattiva idea scegliere la fascia 1, a meno che ovviamente lei non cerca qualcosa di ancor piu' recente (fascia 3) o piu' indietro nel tempo (fascia 2).
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Le e' stata fatta questa domanda perche' ha comunicato di essere interessato a veicoli per uso extraurbano, specialmente ad un suv nuovo.
					Questa domanda aiuta il sistema a focalizzarsi su alcune auto dipendentemente da quanto <<vecchie>> la cerca, quindi questa informazione viene usata per soli fini pratici.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 10) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda10-d3r2
	(domande (domanda 3) (risposta 2))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(parte1 (input s))
	=>
	(printout t "Dato che la cerca usata, da quanti anni vuole che sia in vendita?" crlf)
	(printout t "1- Da circa 5 anni" crlf)
	(printout t "2- Da circa 10 anni" crlf)
	(printout t "3- Al piu' da un paio di anni" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Con questa domanda il sistema vuole capire anche quanto e' disposto a spendere, nonche' capire quali sono i pregi delle auto che sono state lanciate in quei periodi sopra indicati,
					dato che questo significa piu' optional, piu' potenza, piu' affidabilita', nonche' una gestione del consumo del carburante migliore. Infatti, dato che la cerca nuova,
					non sarebbe una cattiva idea scegliere la fascia 1, a meno che ovviamente lei non cerca qualcosa di ancor piu' recente (fascia 3) o piu' indietro nel tempo (fascia 2).
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Le e' stata fatta questa domanda perche' ha comunicato di essere interessato a veicoli per uso extraurbano, specialmente ad un suv nuovo.
					Questa domanda aiuta il sistema a focalizzarsi su alcune auto dipendentemente da quanto <<vecchie>> la cerca, quindi questa informazione viene usata per soli fini pratici.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 10) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda10-d3r3
	(domande (domanda 3) (risposta 3))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(parte1 (input s))
	=>
	(printout t "Visto che la vuole km 0, entro quanti anni vuole che sia stata messa in vendita?" crlf)
	(printout t "1- Da circa 5 anni" crlf)
	(printout t "2- Da circa 10 anni" crlf)
	(printout t "3- Al piu' da un paio di anni" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Con questa domanda il sistema vuole capire anche quanto e' disposto a spendere, nonche' capire quali sono i pregi delle auto che sono state lanciate in quei periodi sopra indicati,
					dato che questo significa piu' optional, piu' potenza, piu' affidabilita', nonche' una gestione del consumo del carburante migliore. Infatti, dato che la cerca nuova,
					non sarebbe una cattiva idea scegliere la fascia 1, a meno che ovviamente lei non cerca qualcosa di ancor piu' recente (fascia 3) o piu' indietro nel tempo (fascia 2).
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Le e' stata fatta questa domanda perche' ha comunicato di essere interessato a veicoli per uso extraurbano, specialmente ad un suv nuovo.
					Questa domanda aiuta il sistema a focalizzarsi su alcune auto dipendentemente da quanto <<vecchie>> la cerca, quindi questa informazione viene usata per soli fini pratici.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 10) (risposta ?r)))
	(printout t "" crlf)
)

(defrule fine-parte1_1
	?p1 <- (parte1 (input s))
	(domande (domanda 1) (risposta ?r1))
	(domande (domanda 2) (risposta ?r2))
	(domande (domanda 3) (risposta ?r3))
	(domande (domanda 4) (risposta ?r4))
	(domande (domanda 5) (risposta ?r5))
	(domande (domanda 6) (risposta ?r6))
	(domande (domanda 7) (risposta 1))
	(domande (domanda 8) (risposta ?r8))
	(domande (domanda 9) (risposta ?r9))
	(domande (domanda 10) (risposta ?r10))
	=>
	(retract ?p1)
	(assert (parte2 (input s)))
)

(defrule fine-parte1_2
	?p1 <- (parte1 (input s))
	(domande (domanda 1) (risposta ?r1))
	(domande (domanda 2) (risposta ?r2))
	(domande (domanda 3) (risposta ?r3))
	(domande (domanda 4) (risposta ?r4))
	(domande (domanda 5) (risposta ?r5))
	(domande (domanda 6) (risposta ?r6))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 9) (risposta ?r9))
	(domande (domanda 10) (risposta ?r10))
	=>
	(retract ?p1)
	(assert (parte2 (input s)))
)

(defrule domanda11
	(declare (salience 5))
	?p1 <- (parte1 (input s))
	(domande (domanda 10) (risposta 2|3))
	=>
	(printout t "E' interessato ad avere a bordo certi sistemi di guida assistita?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Alcuni esempi di sistemi di assistenza alla guida sono: frenata di emergenza, sensori di rilevamento pedoni, sensori di stanchezza, sensori di parcheggio con eventuali
					telecamere anteriori/posteriori/entrambi, manovra di parcheggio semi-automatico, controllo elettronico della stabilita' cosi' come della trazione in curva e su rettilineo, ecc...
					Se lei dovesse usare il SUV prevalentemente per l'uso per cui e' stato progettato, magari questi accessori potrebbero servirle; altrimenti puo' tranquillamente dare parere negativo.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica.
					Questa domanda e' molto importante perche' permette al sistema di recuperare le auto che secondo i suoi gusti hanno o meno sistemi di assistenza alla guida. Ovviamente,
					a seguito di questa domanda il sistema gliene porgera' altre proprio per capire meglio quali sono le sue aspettative a riguardo, costruendo cosi' l'auto per lei ideale.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 11) (risposta ?r)))
	(printout t "" crlf)
	(retract ?p1)
	(assert (parte2 (input s)))
)

(defrule domanda12
	(declare (salience 4))
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	(domande (domanda 11) (risposta 1))
	=>
	(printout t "In particolare, troverebbe interessanti i sensori di parcheggio?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Se dovesse ritenere di volere un aiuto elettronico durante le manovre - pensi a spazi veramente stretti dove dovrebbe ogni volta girare la testa in tutte le direzioni onde
					evitare di ammaccare l'auto - allora le consiglio di rispondere positivamente a questa domanda; se invece dovesse ritenere di poter fare tutto da solo, puo' rispondere negativamente.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
			(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
				uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica; e' inoltre interessato a sistemi di assistenza alla guida.
				Questa domanda puo' fare la differenza nel selezionare le auto, perche' magari alcune hanno i sensori ma non le telecamere e viceversa.
				Cosa vuole fare quindi?" crlf)
			(bind ?r (read))	
			)
		)
	)
	(assert (domande (domanda 12) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda13
	(declare (salience 3))
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	(domande (domanda 11) (risposta 1))
	=>
	(printout t "E per quanto riguarda le telecamere, le vorrebbe?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Se dovesse ritenere di volere un aiuto elettronico durante le manovre - pensi a spazi veramente stretti dove dovrebbe ogni volta girare la testa in tutte le direzioni onde
					evitare di ammaccare l'auto - allora le consiglio di rispondere positivamente a questa domanda; se invece dovesse ritenere di poter fare tutto da solo, puo' rispondere negativamente.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica; e' inoltre interessato a sistemi di assistenza alla guida.
					Questa domanda puo' fare la differenza nel selezionare le auto, perche' magari alcune hanno i sensori ma non le telecamere e viceversa.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 13) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda14
	(declare (salience 2))
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	(domande (domanda 11) (risposta 1))
	=>
	(printout t "Quindi, vorrebbe o meno il parcheggio semi-automatico?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Se dovesse ritenere di volere un aiuto elettronico durante le manovre - pensi a spazi veramente stretti dove dovrebbe ogni volta girare la testa in tutte le direzioni onde
					evitare di ammaccare l'auto - allora le consiglio di rispondere positivamente a questa domanda; se invece dovesse ritenere di poter fare tutto da solo, puo' rispondere negativamente.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica; e' inoltre interessato a sistemi di assistenza alla guida.
					Questa domanda puo' fare la differenza nel selezionare le auto, perche' non tutti i SUV hanno a bordo sistemi cosi' avanzati.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 14) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda15
	(declare (salience 2))
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	(domande (domanda 11) (risposta 1))
	=>
	(printout t "Vorrebbe avere il sistema di controllo elettronico della stabilita'?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Questo tipo di sistema permette di gestire meglio la stabilita' dell'auto in curve strette a velocita' elevata, cosi' come gestisce la coppia anche nel caso in cui
					dovesse trasportare carichi medio-pesanti e questi durante le curve tendono a spostarsi sul lato opposto, dando al conducente la sensazione di avere ancora l'auto sotto controllo.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica; e' inoltre interessato a sistemi di assistenza alla guida.
					Questa domanda puo' fare la differenza nel selezionare le auto, perche' non tutti i SUV hanno a bordo sistemi cosi' avanzati.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 15) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda16
	(declare (salience 2))
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	(domande (domanda 11) (risposta 1))
	=>
	(printout t "Vorrebbe avere il sistema di mantenimento della carreggiata?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Questo tipo di sistema permette di gestire meglio la stabilita' dell'auto in curve strette a velocita' elevata, cosi' come gestisce la coppia anche nel caso in cui
					dovesse trasportare carichi medio-pesanti e questi durante le curve tendono a spostarsi sul lato opposto, dando al conducente la sensazione di avere ancora l'auto sotto controllo.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
				uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica; e' inoltre interessato a sistemi di assistenza alla guida.
				Questa domanda puo' fare la differenza nel selezionare le auto, perche' non tutti i SUV hanno a bordo sistemi cosi' avanzati.
				Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 16) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda17
	(declare (salience 2))
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	(domande (domanda 11) (risposta 1))
	=>
	(printout t "Vorrebbe avere il sistema di riconoscimento dei pedoni?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Questo sistema e' piuttosto avanzato, pertanto alcune case automobilistiche potrebbero inserirlo in linee medio-costose se non costose (le piu' alte praticamente).
					Quindi, quando risponde alla domanda, si renda conto che nel volerlo potrebbe volere implicitamente un'auto piu' costosa che comprende altri accessori, piu' o meno di suo gradimento.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica; e' inoltre interessato a sistemi di assistenza alla guida.
					Questa domanda puo' fare la differenza nel selezionare le auto, perche' non tutti i SUV hanno a bordo sistemi cosi' avanzati.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 17) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda18
	(declare (salience 2))
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	(domande (domanda 11) (risposta 1))
	=>
	(printout t "Vorrebbe avere il sistema di riconoscimento della fatica?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "Questo sistema e' piuttosto avanzato, pertanto alcune case automobilistiche potrebbero inserirlo in linee medio-costose se non costose (le piu' alte praticamente).
					Quindi, quando risponde alla domanda, si renda conto che nel volerlo potrebbe volere implicitamente un'auto piu' costosa che comprende altri accessori, piu' o meno di suo gradimento.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica; e' inoltre interessato a sistemi di assistenza alla guida.
					Questa domanda puo' fare la differenza nel selezionare le auto, perche' non tutti i SUV hanno a bordo sistemi cosi' avanzati.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 18) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda19
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	=>
	(printout t "Tra il cambio manuale e quello automatico quale preferisce?" crlf)
	(printout t "1- Quello manuale" crlf)
	(printout t "2- Quello automatico" crlf)
	(printout t "3- Indifferente" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Da un punto di vista pratico, il cambio automatico e' appunto automatico, quindi la macchina ingrana le marce da sola a certi giri del motore. Tuttavia, il cambio automatico costa in genere
					un paio di migliaia di euro in piu' sul prezzo di listino, quindi si regoli; inoltre, si evince che le auto col cambio automatico producano piu' emissioni rispetto a quelle col cambio manuale.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica.
					Questa domanda puo' fare la differenza nel selezionare le auto, perche' non tutti i SUV hanno a bordo sistemi cosi' avanzati.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 19) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda20
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	=>
	(printout t "E' interessato alla trazione anteriore o integrale (nota anche come 4x4)?" crlf)
	(printout t "1- Anteriore" crlf)
	(printout t "2- Integrale" crlf)
	(printout t "3- Indifferente" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Da un punto di vista pratico, il cambio automatico e' appunto automatico, quindi la macchina ingrana le marce da sola a certi giri del motore. Tuttavia, il cambio automatico costa in genere
					un paio di migliaia di euro in piu' sul prezzo di listino, quindi si regoli; inoltre, si evince che le auto col cambio automatico producano piu' emissioni rispetto a quelle col cambio manuale.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica.
					Questa domanda puo' fare la differenza nel selezionare le auto, perche' non tutti i SUV hanno a bordo sistemi cosi' avanzati.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 20) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda21
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	=>
	(printout t "Tra cerchi in lega e acciaio, quali preferisce?" crlf)
	(printout t "1- Lega" crlf)
	(printout t "2- Acciaio" crlf)
	(printout t "3- Help" crlf)
	(printout t "4- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 3) (eq ?r 4) (< ?r 1) (> ?r 4))
		(switch ?r
			(case 3 then
				(printout t "E' risaputo che i cerchi in acciaio sono piu' robusti ma meno raffinati, ovviamente ad un costo compreso nel pacchetto di serie. Al contrario, invece, i cerchi in lega sono piu'
					delicati ma piu' raffinati, e solitamente sono presenti di serie su linee di costo medio-alte.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 4 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica.
					Se volesse i cerchi in acciaio stiamo parlando di fasce di basso livello; altrimenti di fasce medio-alte alte.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 21) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda22
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	=>
	(printout t "Vorrebbe a bordo un climatizzatore manuale o bi-zona?" crlf)
	(printout t "1- Manuale" crlf)
	(printout t "2- Bi-zona" crlf)
	(printout t "3- Indifferente" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Questo genere di accessorio e' solitamente di serie a partire da una linea medio-alta, il che significa che nella linea ci sono altri accessori di serie che potrebbe o meno gradire.
					Inoltre, essendo una linea di un certo rilievo, anche il prezzo automaticamente lievita, quindi presti attenzione a questo genere di domande.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica.
					Essendo un optional di serie, e' importante per il sistema capire se a lei farebbe piacere averlo di serie oppure no.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
		)
	)
	(assert (domande (domanda 22) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda23
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	=>
	(printout t "Le piacerebbe aprire il portabagagli con un movimento del piede sotto il paraurti?" crlf)
	(printout t "1- Si" crlf)
	(printout t "2- No" crlf)
	(printout t "3- Indifferente" crlf)
	(printout t "4- Help" crlf)
	(printout t "5- Perche' mi fa questa domanda?" crlf)
	(bind ?r (read))
	(while (or (eq ?r 4) (eq ?r 5) (< ?r 1) (> ?r 5))
		(switch ?r
			(case 4 then
				(printout t "Questo genere di accessorio e' solitamente di serie a partire da una linea medio-alta, il che significa che nella linea ci sono altri accessori di serie che potrebbe o meno gradire.
					Inoltre, essendo una linea di un certo rilievo, anche il prezzo automaticamente lievita, quindi presti attenzione a questo genere di domande.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)
			(case 5 then
				(printout t "Il sistema le ha posto questa domanda oerche' ha verificato che cerca un'auto con alimentazione diesel, per uso extraurbano in particolare suv, nuova,
					uscita sul mercato al piu' da un paio di anni, ma senza che lei abbia espresso preferenza per una casa automobilistica.
					Essendo un optional di serie, e' importante per il sistema capire se a lei farebbe piacere averlo di serie oppure no.
					Cosa vuole fare quindi?" crlf)
				(bind ?r (read))
			)		
		)
	)
	(assert (domande (domanda 23) (risposta ?r)))
	(printout t "" crlf)
)

(defrule domanda24
	(declare (salience -2))
	(parte2 (input s))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 10) (risposta 2|3))
	=>
	(printout t "In quale fascia di prezzo si colloca?" crlf)
	(printout t "1- Sotto i 30mila euro" crlf)
	(printout t "2- Tra i 30mila e i 45mila euro" crlf)
	(printout t "3- Tra i 45mila e i 60mila euro" crlf)
	(printout t "4- Tra i 60mila e i 75mila euro" crlf)
	(printout t "5- Oltre i 75mila euro" crlf)
	(assert (domande (domanda 24) (risposta (read))))
)

(defrule fine_parte2-1
	?p2 <- (parte2 (input s))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta ?))
	(domande (domanda 13) (risposta ?))
	(domande (domanda 14) (risposta ?))
	(domande (domanda 15) (risposta ?))
	(domande (domanda 16) (risposta ?))
	(domande (domanda 17) (risposta ?))
	(domande (domanda 18) (risposta ?))
	(domande (domanda 19) (risposta ?))
	(domande (domanda 20) (risposta ?))
	(domande (domanda 21) (risposta ?))
	(domande (domanda 22) (risposta ?))
	(domande (domanda 23) (risposta ?))
	(domande (domanda 24) (risposta ?))
	=>
	(retract ?p2)
	(while (neq (get-strategy) depth)
		(set-strategy depth)
	)
)

(defrule fine_parte2-2
	?p2 <- (parte2 (input s))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta ?))
	(domande (domanda 20) (risposta ?))
	(domande (domanda 21) (risposta ?))
	(domande (domanda 22) (risposta ?))
	(domande (domanda 23) (risposta ?))
	(domande (domanda 24) (risposta ?))
	=>
	(retract ?p2)
	(while (neq (get-strategy) depth)
		(set-strategy depth)
	)
)

(deffunction sort-emissioni (?a1 ?a2)
	(> (fact-slot-value ?a1 motore_emissioni) (fact-slot-value ?a2 motore_emissioni))
)

(deffunction sort-kw (?a1 ?a2)
	(> (fact-slot-value ?a1 motore_kw) (fact-slot-value ?a2 motore_kw))
)

(deffunction sort-bagagliaio_litri_min (?a1 ?a2)
	(< (fact-slot-value ?a1 bagagliaio_litri_min) (fact-slot-value ?a2 bagagliaio_litri_min))
)

(deffunction sort-bagagliaio_litri_max (?a1 ?a2)
	(< (fact-slot-value ?a1 bagagliaio_litri_max) (fact-slot-value ?a2 bagagliaio_litri_max))
)

(deffunction sort-motore_carburante_ciclo_urbano (?a1 ?a2)
	(> (fact-slot-value ?a1 motore_carburante_ciclo_urbano) (fact-slot-value ?a2 motore_carburante_ciclo_urbano))
)

(deffunction sort-motore_carburante_ciclo_extraurbano (?a1 ?a2)
	(> (fact-slot-value ?a1 motore_carburante_ciclo_extraurbano) (fact-slot-value ?a2 motore_carburante_ciclo_extraurbano))
)

(deffunction sort-motore_rapporti (?a1 ?a2)
	(< (fact-slot-value ?a1 motore_rapporti) (fact-slot-value ?a2 motore_rapporti))
)

(deffunction sort-motore_cilindri (?a1 ?a2)
	(< (fact-slot-value ?a1 motore_cilindri) (fact-slot-value ?a2 motore_cilindri))
)

(deffunction sort-motore_litri (?a1 ?a2)
	(< (fact-slot-value ?a1 motore_litri) (fact-slot-value ?a2 motore_litri))
)

(deffunction sort-prezzo_totale (?a1 ?a2)
	(< (fact-slot-value ?a1 prezzo_max) (fact-slot-value ?a2 prezzo_max))
)

(defrule caso1
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva di 30mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso2
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda categoria) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva di 30mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso3
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima categoria) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva di 30mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso4
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva tra i 30mila e i 45mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso5
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva tra i 30mila e i 45mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso6
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva tra i 30mila e i 45mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso7
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva tra i 45mila e i 60mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso8
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva tra i 45mila e i 60mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso9
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva tra i 45mila e i 60mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso10
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva tra i 60mila e i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso11
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva 60mila e i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso12
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 1200) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva 60mila e i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso13
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva oltre i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso14
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva oltre i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso15
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 1))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 1))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 1))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li voleva tutti (ovviamente di serie), successivamente ha anche scelto cambio automatico,
						trazione anteriore, cerchi in lega, climatizzatore manuale e apertura portabagagli col piede, entro una spesa complessiva oltre i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso16
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, in una fascia di prezzo che non supera i 30mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso17
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, in una fascia di prezzo che non supera i 30mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso18
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, in una fascia di prezzo che non supera i 30mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso19
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 30mila e i 45mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso20
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 30mila e i 45mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso21
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 30mila e i 45mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso22
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 45mila e i 60mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso23
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 45mila e i 60mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso24
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 45mila e i 60mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso25
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 60000.0) (<= ?a:prezzo_max 75000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 60mila e i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso26
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 60000.0) (<= ?a:prezzo_max 75000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 60mila e i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso27
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 60000.0) (<= ?a:prezzo_max 75000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo varia tra i 60mila e i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso28
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (>= ?a:prezzo_max 75000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la entry level) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo supera i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso29
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (>= ?a:prezzo_max 75000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo supera i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso30
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 1))
	(domande (domanda 16) (risposta 1))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 1))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 2))
	(domande (domanda 22) (risposta 1))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (>= ?a:prezzo_max 75000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi-automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE)
		(neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in acciaio" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto l'ultima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scelti tutti (ovviamente di serie) tranne le telecamere e il sistema di riconoscimento dei pedoni,
						successivamente ha anche scelto cambio automatico, trazione anteriore, cerchi in acciaio e climatizzatore manuale, il cui costo complessivo supera i 75mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso31
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso32
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso33
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 1))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso34
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso35
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso36
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso37
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso38
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso39
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 45000.0) (<= ?a:prezzo_max 60000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 45000.0) (<= ?acc:prezzo_totale 60000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso40
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 60000.0) (<= ?a:prezzo_max 75000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso41
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 60000.0) (<= ?a:prezzo_max 75000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso42
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 4))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (and (>= ?a:prezzo_max 60000.0) (<= ?a:prezzo_max 75000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (and (>= ?acc:prezzo_totale 60000.0) (<= ?acc:prezzo_totale 75000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso43
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 1))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (>= ?a:prezzo_max 75000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso44
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 2))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 100) (<= ?a:motore_cavalli 120)) (<= (- 2018 ?a:anno_uscita) 2) (>= ?a:prezzo_max 75000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso45
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 5))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (>= ?a:motore_cavalli 120) (<= (- 2018 ?a:anno_uscita) 2) (>= ?a:prezzo_max 75000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio manuale" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (>= ?acc:prezzo_totale 75000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la seconda fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso46
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 2))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (> ?a:motore_cavalli 120) (and (>= ?a:prezzo_min 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio automatico" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (or (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
			(assert (ritrattazione (input s)))
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		(assert (ritrattazione (input s)))
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori n)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto s) (accessori s)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la massima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso47
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 2))
	(domande (domanda 19) (risposta 2))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 3))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (> ?a:motore_cavalli 120) (and (>= ?a:prezzo_min 30000.0) (<= ?a:prezzo_max 45000.0)))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Cambio automatico" ?acc:accessori) FALSE) (neq (str-index "Trazione anteriore" ?acc:accessori) FALSE)
		(neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Climatizzatore automatico bi zona" ?acc:accessori) FALSE)
		(neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE) (or (>= ?acc:prezzo_totale 30000.0) (<= ?acc:prezzo_totale 45000.0)))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
			(assert (ritrattazione (input s)))
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		(assert (ritrattazione (input s)))
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori n)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto s) (accessori s)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto come volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la massima fascia) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei li ha scartati tutti, successivamente ha anche scelto cambio manuale, trazione anteriore,
						cerchi in lega e climatizzatore automatico bi zona.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\""))
		)
	)
)

(defrule caso48
	(domande (domanda 2) (risposta 1|2|3|4))
	(domande (domanda 4) (risposta 2))
	(domande (domanda 6) (risposta 2))
	(domande (domanda 7) (risposta 2))
	(domande (domanda 3) (risposta 1))
	(domande (domanda 1) (risposta 3))
	(domande (domanda 5) (risposta 1))
	(domande (domanda 9) (risposta 1))
	(domande (domanda 10) (risposta 3))
	(domande (domanda 11) (risposta 1))
	(domande (domanda 12) (risposta 1))
	(domande (domanda 13) (risposta 2))
	(domande (domanda 14) (risposta 1))
	(domande (domanda 15) (risposta 2))
	(domande (domanda 16) (risposta 2))
	(domande (domanda 17) (risposta 2))
	(domande (domanda 18) (risposta 2))
	(domande (domanda 19) (risposta 1))
	(domande (domanda 20) (risposta 1))
	(domande (domanda 21) (risposta 1))
	(domande (domanda 22) (risposta 2))
	(domande (domanda 23) (risposta 2))
	(domande (domanda 24) (risposta 2))
	=>
	(bind ?numRisultati 0)
	(bind ?idImg -1)
	(bind ?auto (find-all-facts ((?a auto)) (and (eq ?a:condizioni nuova) (eq ?a:motore_alimentazione diesel) (eq ?a:tipologia suv) (and (>= ?a:motore_cavalli 80) (<= ?a:motore_cavalli 100)) (<= (- 2018 ?a:anno_uscita) 2) (<= ?a:prezzo_max 30000.0))))
	(bind ?accessori (find-all-facts ((?acc linee_di_accessori)) (and (neq (str-index "Sensori di parcheggio" ?acc:accessori) FALSE) (neq (str-index "Telecamere" ?acc:accessori) FALSE)
		(neq (str-index "Parcheggio semi automatico" ?acc:accessori) FALSE) (neq (str-index "Controllo elettronico della stabilita'" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di mantenimento della carreggiata" ?acc:accessori) FALSE) (neq (str-index "Sistema di riconoscimento pedoni" ?acc:accessori) FALSE)
		(neq (str-index "Sistema di riconoscimento della fatica" ?acc:accessori) FALSE) (neq (str-index "Cambio manuale" ?acc:accessori) FALSE)
		(neq (str-index "Trazione anteriore" ?acc:accessori) FALSE) (neq (str-index "Cerchi in lega" ?acc:accessori) FALSE) (neq (str-index "Apertura portabagagli col piede" ?acc:accessori) FALSE)
		(neq (str-index "Climatizzatore manuale" ?acc:accessori) FALSE) (<= ?acc:prezzo_totale 30000.0))))
	(bind ?quantiAccessori (length$ ?accessori))
	(bind ?quanteAuto (length$ ?auto))
	(if (and (>= ?quantiAccessori 1) (>= ?quanteAuto 1)) then
		(bind ?auto (sort sort-emissioni ?auto))
		(bind ?auto (sort sort-kw ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_min ?auto))
		(bind ?auto (sort sort-bagagliaio_litri_max ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_urbano ?auto))
		(bind ?auto (sort sort-motore_carburante_ciclo_extraurbano ?auto))
		(bind ?auto (sort sort-motore_rapporti ?auto))
		(bind ?auto (sort sort-motore_cilindri ?auto))
		(bind ?auto (sort sort-motore_litri ?auto))
		(bind ?trovato 0)
		(do-for-all-facts ((?accessori linee_di_accessori)) (neq ?trovato 1)
			(progn$ (?auto ?auto)
				(if (eq ?accessori:id_auto (fact-slot-value ?auto id)) then
					(bind ?idImg (fact-slot-value ?accessori id))
					(printout t "" crlf)
					(printout t "Ecco un'auto che riflette le sue preferenze:" crlf)
					(printout t "" crlf)
					(printout t "MARCA: " tab (fact-slot-value ?auto marca) crlf)
					(printout t "MODELLO: " tab (fact-slot-value ?auto modello) crlf)
					(printout t "NOME LINEA: " tab ?accessori:nome_linea crlf)
					(printout t "BAGAGLIAIO: da " (fact-slot-value ?auto bagagliaio_litri_min) " a " (fact-slot-value ?auto bagagliaio_litri_max) " litri" crlf)
					(printout t "" crlf)
					(printout t "Si tratta di un motore " (fact-slot-value ?auto motore_nome) " da " (fact-slot-value ?auto motore_litri) " litri, " (fact-slot-value ?auto motore_kw) "kw con le seguenti emissioni:" crlf)
					(printout t "EMISSIONI MOTORE: " (fact-slot-value ?auto motore_emissioni) " g/km" crlf)
					(printout t "CONSUMO A CICLO URBANO: " (fact-slot-value ?auto motore_carburante_ciclo_urbano) " g/km" crlf)
					(printout t "CONSUMO A CICLO EXTRAURBANO: " (fact-slot-value ?auto motore_carburante_ciclo_extraurbano) " g/km" crlf)
					(printout t "" crlf)
					(printout t "PREZZO COMPLESSIVO: " tab ?accessori:prezzo_totale crlf)
					(printout t "" crlf)
				(bind ?trovato 1)
				(bind ?numRisultati 1)
				)
			)
		)
		(if (eq ?trovato 0) then
			(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
		)
	else
		(printout t "Mi spiace, non ci sono auto con le caratteristiche da lei desiderate..." crlf)
	)
	(printout t "Vuole avviare la fase di ritrattazione oppure vuole terminare l'esecuzione del programma?" crlf)
	(printout t "1- Si, avvio la fase di ritrattazione" crlf)
	(printout t "2- No, termino l'esecuzione del programma" crlf)
	(printout t "" crlf)
	(bind ?sceltaFinale (read))
	(switch ?sceltaFinale
		(case 1 then
			(assert (ritrattazione (input s)))
			(if (>= ?quantiAccessori 1) then
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto s) (accessori s)))
				else
					(assert (empty (auto s) (accessori n)))
				)
			else
				(if (>= ?quanteAuto 1) then
					(assert (empty (auto n) (accessori s)))
				else
					(assert (empty (auto n) (accessori n)))
				)
			)
		)
		(case 2 then
			(printout t "Vuole sapere come il programma e' arrivato a consigliarle quest'auto specifica?" crlf)
			(printout t "1- Si" crlf)
			(printout t "2- No" crlf)
			(printout t "" crlf)
			(bind ?soluzione (read))
			(switch ?soluzione
				(case 1 then
					(printout t "All'inizio il sistema le ha fatto domande sulla sua eta', su quale fosse l'utilizzo dell'auto e quale auto lei stesse cercando in particolare, cosi' come se avesse
						preferenze per certe case automobilistiche (ma non ne ha). Allora il sistema le ha chiesto in che condizioni volesse l'auto (e lei ha detto di volerla nuova), se ci tenesse ad un
						bagagliaio ampio (e ha detto di si) nonche' alla potenza (e ha scelto la fascia alta) e alla data di uscita in particolare (lei l'ha voluta veramente recente). Quindi, il
						sistema le ha chiesto se fosse interessato ai sistemi di guida assistita, e lei non ne ha voluto nessuno; successivamente ha anche scelto cambio manuale,
						trazione anteriore, cerchi in lega e climatizzatore automatico bi zona, in una spesa complessiva tra i 30mila euro e i 45mila euro.
						Pertanto, con tutte queste informazioni, il sistema ha recuperato innanzitutto una auto che avesse questi accessori di serie (quindi uno specifico modello a prescindere di
						quale linea facesse parte); se esisteva, il sistema ha recuperato il modello di auto (caratteristiche tecniche) di cui fa parte, restituendo tutti i dettagli a video." crlf)
					(halt)
				)
				(case 2 then
					(printout t "Il programma termina ora l'esecuzione. Grazie per l'utilizzo!" crlf)
					(halt)
				)
				(default none)
			)
		)
		(default none)
	)
	(if (eq ?numRisultati 1) then
		(printout t "Vuole vedere una fotografia dell'auto in questione?" crlf)
		(printout t "1- Si" crlf)
		(printout t "2- No" crlf)
		(bind ?img (read))
		(if (eq ?img 1) then
			(system (str-cat (str-cat "start \"Imamgine auto\" \"./immagini/" ?idImg) ".jpg\"")) ;;idImg è la variabile che memorizza l'id dell'auto
		)
	)
)

(defrule ritrattazione1
	(declare (salience 20))
	?r <- (ritrattazione (input s))
	(empty (auto ?n1) (accessori ?n2))
	?d1 <- (domande (domanda 1) (risposta ?r1))
	?d3 <- (domande (domanda 3) (risposta ?r3))
	?d4 <- (domande (domanda 4) (risposta ?r4))
	?d5 <- (domande (domanda 5) (risposta ?r5))
	?d6 <- (domande (domanda 6) (risposta ?r6))
	?d7 <- (domande (domanda 7) (risposta 2))
	?d9 <- (domande (domanda 9) (risposta ?r9))
	?d10 <- (domande (domanda 10) (risposta ?r10))
	?d11 <- (domande (domanda 11) (risposta 1))
	?d12 <- (domande (domanda 12) (risposta ?r12))
	?d13 <- (domande (domanda 13) (risposta ?r13))
	?d14 <- (domande (domanda 14) (risposta ?r14))
	?d15 <- (domande (domanda 15) (risposta ?r15))
	?d16 <- (domande (domanda 16) (risposta ?r16))
	?d17 <- (domande (domanda 17) (risposta ?r17))
	?d18 <- (domande (domanda 18) (risposta ?r18))
	?d19 <- (domande (domanda 19) (risposta ?r19))
	?d20 <- (domande (domanda 20) (risposta ?r20))
	?d21 <- (domande (domanda 21) (risposta ?r21))
	?d22 <- (domande (domanda 22) (risposta ?r22))
	?d23 <- (domande (domanda 23) (risposta ?r23))
	?d24 <- (domande (domanda 24) (risposta ?r24))
	=>
	(printout t "Ha appena dato il via alla fase di ritrattazione, in cui puo' modificare le risposte che ha dato alle precedenti domande.
		Puo' modificare la risposta per ciascuna delle domande che seguono attraverso l'inserimento da tastiera del rispettivo ID; prema 0 per terminare la fase di ritrattazione." crlf)
	(if (eq ?n1 0) then
		(if (eq ?n2 0) then
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' non ci sono auto con le caratteristiche tecniche che cerca, nonche' gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte ad alcune domande del genere." crlf)
		else
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' non ci sono auto con le caratteristiche tecniche che cerca, ma esistono linee di auto con gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte alle domande circa le caratteristiche tecniche dell'auto (da ID 1 a ID 10)." crlf)
		)
	else
		(if (eq ?n2 1) then
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' sebbene ci siano auto con le caratteristiche tecniche che cerca, non esistono linee di auto con gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte alle domande circa gli accessori e i componenti personalizzabili (da ID 11 a ID 24)." crlf)
		)
	)
	(bind ?ciclo -1)
	(bind ?R1 ?r1)
	(bind ?R3 ?r3)
	(bind ?R4 ?r4)
	(bind ?R5 ?r5)
	(bind ?R6 ?r6)
	(bind ?R7 2)
	(bind ?R9 ?r9)
	(bind ?R10 ?r10)
	(bind ?R11 1)
	(bind ?R12 ?r12)
	(bind ?R13 ?r13)
	(bind ?R14 ?r14)
	(bind ?R15 ?r15)
	(bind ?R16 ?r16)
	(bind ?R17 ?r17)
	(bind ?R18 ?r18)
	(bind ?R19 ?r19)
	(bind ?R20 ?r20)
	(bind ?R21 ?r21)
	(bind ?R22 ?r22)
	(bind ?R23 ?r23)
	(bind ?R24 ?r24)
	(while (neq ?ciclo 0)
		(printout t "[ID: 1]" tab "A quale fascia di potenza e' interessato? (ha risposto " ?r1 ")" crlf)
		(printout t "[ID: 3]" tab "Vorrebbe l'auto nuova? (ha risposto " ?r3 ")" crlf)
		(printout t "[ID: 4]" tab "L'auto le serve per spostarsi in citta' o fuori citta', usualmente? (ha risposto " ?r4 ")" crlf)
		(printout t "[ID: 5]" tab "Che tipo di alimentazione preferirebbe per l'auto? (ha risposto " ?r5 ")" crlf)
		(printout t "[ID: 6]" tab "Che tipo di auto vorrebbe? (ha risposto " ?r6 ")" crlf)
		(printout t "[ID: 7]" tab "Ha qualche preferenza in termini di case automobilistiche? (ha risposto 2)" crlf)
		(printout t "[ID: 9]" tab "Vuole che il bagagliaio sia capiente? (ha risposto " ?r9 ")" crlf)
		(printout t "[ID: 10]" tab "Vuole un'auto che e' presente da poco sul mercato o l'inverso? (ha risposto " ?r10 ")" crlf)
		(printout t "[ID: 11]" tab "E' interessato ai sistemi di guida assistita? (ha risposto 1)" crlf)
		(printout t "[ID: 12]" tab "In particolare, troverebbe interessanti i sensori di parcheggio? (ha risposto " ?r12 ")" crlf)
		(printout t "[ID: 13]" tab "E per quanto riguarda le telecamere, le vorrebbe? (ha risposto " ?r13 ")" crlf)
		(printout t "[ID: 14]" tab "Quindi, vorrebbe o meno il parcheggio semi-automatico? (ha risposto " ?r14 ")" crlf)
		(printout t "[ID: 15]" tab "Vorrebbe avere il sistema di controllo elettronico della stabilita'? (ha risposto " ?r15 ")" crlf)
		(printout t "[ID: 16]" tab "Vorrebbe avere il sistema di mantenimento della carreggiata? (ha risposto " ?r16 ")" crlf)
		(printout t "[ID: 17]" tab "Vorrebbe avere il sistema di riconoscimento dei pedoni? (ha risposto " ?r17 ")" crlf)
		(printout t "[ID: 18]" tab "Vorrebbe avere il sistema di riconoscimento della fatica? (ha risposto " ?r18 ")" crlf)
		(printout t "[ID: 19]" tab "Tra il cambio manuale e quello automatico quale preferisce? (ha risposto " ?r19 ")" crlf)
		(printout t "[ID: 20]" tab "E' interessato alla trazione anteriore o integrale (nota anche come 4x4)? (ha risposto " ?r20 ")" crlf)
		(printout t "[ID: 21]" tab "Tra cerchi in lega e acciaio, quali preferisce? (ha risposto " ?r21 ")" crlf)
		(printout t "[ID: 22]" tab "Vorrebbe a bordo un climatizzatore manuale o bi-zona? (ha risposto " ?r22 ")" crlf)
		(printout t "[ID: 23]" tab "Le piacerebbe aprire il portabagagli con un movimento del piede sotto il paraurti? (ha risposto " ?r23 ")" crlf)
		(printout t "[ID: 24]" tab "In quale fascia di prezzo si colloca? (ha risposto " ?r24 ")" crlf)
		(bind ?ciclo (read))
		(switch ?ciclo
			(case 1 then
				(printout t "1- Tra gli 80 e i 100cv" crlf)
				(printout t "2- Tra i 100 e i 120cv" crlf)
				(printout t "3- Oltre i 120cv" crlf)
				(bind ?R1 (read))
			)
			(case 3 then
				(printout t "1- Si, la prendo nuova" crlf)
				(printout t "2- No, la prendo usata" crlf)
				(printout t "3- No, la prendo km 0" crlf)
				(bind ?R3 (read))
			)
			(case 4 then
				(printout t "1- Per spostarmi in citta'" crlf)
				(printout t "2- Per spostarmi fuori citta'" crlf)
				(bind ?R4 (read))
			)
			(case 5 then
				(printout t "1- Diesel" crlf)
				(printout t "2- Ibrida" crlf)
				(printout t "3- Full-electric" crlf)
				(bind ?R5 (read))
			)
			(case 6 then
				(printout t "1- Berlina" crlf)
				(printout t "2- SUV" crlf)
				(printout t "3- Sportiva" crlf)
				(printout t "4- Station-wagon" crlf)
				(printout t "5- Compatta" crlf)
				(printout t "6- Lusso" crlf)
				(bind ?R6 (read))
			)
			(case 7 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R7 (read))
			)
			(case 9 then
				(printout t "1- Si" crlf)
				(printout t "2- Non necessariamente" crlf)
				(bind ?R9 (read))
			)
			(case 10 then
				(printout t "1- No, una che e' sul mercato da circa 5 anni" crlf)
				(printout t "2- No, una che e' sul mercato da circa 10 anni" crlf)
				(printout t "3- Si, una che e' sul mercato da un paio di anni" crlf)
				(bind ?R10 (read))
			)
			(case 11 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R11 (read))
			)
			(case 12 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R12 (read))
			)
			(case 13 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R13 (read))
			)
			(case 14 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R14 (read))
			)
			(case 15 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R15 (read))
			)
			(case 16 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R16 (read))
			)
			(case 17 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R17 (read))
			)
			(case 18 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R18 (read))
			)
			(case 19 then
				(printout t "1- Manuale" crlf)
				(printout t "2- Automatico" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R19 (read))
			)
			(case 20 then
				(printout t "1- Anteriore" crlf)
				(printout t "2- Integrale" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R20 (read))
			)
			(case 21 then
				(printout t "1- Lega" crlf)
				(printout t "2- Acciaio" crlf)
				(bind ?R21 (read))
			)
			(case 22 then
				(printout t "1- Manuale" crlf)
				(printout t "2- Automatico" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R22 (read))
			)
			(case 23 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R23 (read))
			)
			(case 24 then
				(printout t "1- Non oltre i 30mila euro" crlf)
				(printout t "2- Tra i 30mila e i 45mila euro" crlf)
				(printout t "3- Tra i 45mila e i 60mila euro" crlf)
				(printout t "4- Tra i 60mila e i 75mila euro" crlf)
				(printout t "5- Oltre i 75mila euro" crlf)
				(bind ?R24 (read))
			)
			(default none)
		)
	)
	(retract ?d1)
	(retract ?d3)
	(retract ?d4)
	(retract ?d5)
	(retract ?d6)
	(retract ?d7)
	(retract ?d9)
	(retract ?d10)
	(retract ?d11)
	(retract ?d12)
	(retract ?d13)
	(retract ?d14)
	(retract ?d15)
	(retract ?d16)
	(retract ?d17)
	(retract ?d18)
	(retract ?d19)
	(retract ?d20)
	(retract ?d21)
	(retract ?d22)
	(retract ?d23)
	(retract ?d24)
	(assert (domande (domanda 1) (risposta ?R1)))
	(assert (domande (domanda 3) (risposta ?R3)))
	(assert (domande (domanda 4) (risposta ?R4)))
	(assert (domande (domanda 5) (risposta ?R5)))
	(assert (domande (domanda 6) (risposta ?R6)))
	(assert (domande (domanda 7) (risposta ?R7)))
	(assert (domande (domanda 9) (risposta ?R9)))
	(assert (domande (domanda 10) (risposta ?R10)))
	(assert (domande (domanda 11) (risposta ?R11)))
	(assert (domande (domanda 12) (risposta ?R12)))
	(assert (domande (domanda 13) (risposta ?R13)))
	(assert (domande (domanda 14) (risposta ?R14)))
	(assert (domande (domanda 15) (risposta ?R15)))
	(assert (domande (domanda 16) (risposta ?R16)))
	(assert (domande (domanda 17) (risposta ?R17)))
	(assert (domande (domanda 18) (risposta ?R18)))
	(assert (domande (domanda 19) (risposta ?R19)))
	(assert (domande (domanda 20) (risposta ?R20)))
	(assert (domande (domanda 21) (risposta ?R21)))
	(assert (domande (domanda 22) (risposta ?R22)))
	(assert (domande (domanda 23) (risposta ?R23)))
	(assert (domande (domanda 24) (risposta ?R24)))
	(retract ?r)
	(printout t "Il sistema sta elaborando le sue scelte..." crlf)
)

(defrule ritrattazione2
	(declare (salience 20))
	?r <- (ritrattazione (input s))
	(empty (auto ?n1) (accessori ?n2))
	?d1 <- (domande (domanda 1) (risposta ?r1))
	?d3 <- (domande (domanda 3) (risposta ?r3))
	?d4 <- (domande (domanda 4) (risposta ?r4))
	?d5 <- (domande (domanda 5) (risposta ?r5))
	?d6 <- (domande (domanda 6) (risposta ?r6))
	?d7 <- (domande (domanda 7) (risposta 1))
	?d8 <- (domande (domanda 8) (risposta ?r8))
	?d9 <- (domande (domanda 9) (risposta ?r9))
	?d10 <- (domande (domanda 10) (risposta ?r10))
	?d11 <- (domande (domanda 11) (risposta 1))
	?d12 <- (domande (domanda 12) (risposta ?r12))
	?d13 <- (domande (domanda 13) (risposta ?r13))
	?d14 <- (domande (domanda 14) (risposta ?r14))
	?d15 <- (domande (domanda 15) (risposta ?r15))
	?d16 <- (domande (domanda 16) (risposta ?r16))
	?d17 <- (domande (domanda 17) (risposta ?r17))
	?d18 <- (domande (domanda 18) (risposta ?r18))
	?d19 <- (domande (domanda 19) (risposta ?r19))
	?d20 <- (domande (domanda 20) (risposta ?r20))
	?d21 <- (domande (domanda 21) (risposta ?r21))
	?d22 <- (domande (domanda 22) (risposta ?r22))
	?d23 <- (domande (domanda 23) (risposta ?r23))
	?d24 <- (domande (domanda 24) (risposta ?r24))
	=>
	(printout t "Ha appena dato il via alla fase di ritrattazione, in cui puo' modificare le risposte che ha dato alle precedenti domande.
		Puo' modificare la risposta per ciascuna delle domande che seguono attraverso l'inserimento da tastiera del rispettivo ID; prema 0 per terminare la fase di ritrattazione." crlf)
	(if (eq ?n1 0) then
		(if (eq ?n2 0) then
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' non ci sono auto con le caratteristiche tecniche che cerca, nonche' gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte ad alcune domande del genere." crlf)
		else
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' non ci sono auto con le caratteristiche tecniche che cerca, ma esistono linee di auto con gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte alle domande circa le caratteristiche tecniche dell'auto (da ID 1 a ID 10)." crlf)
		)
	else
		(if (eq ?n2 1) then
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' sebbene ci siano auto con le caratteristiche tecniche che cerca, non esistono linee di auto con gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte alle domande circa gli accessori e i componenti personalizzabili (da ID 11 a ID 24)." crlf)
		)
	)
	(bind ?ciclo -1)
	(bind ?R1 ?r1)
	(bind ?R3 ?r3)
	(bind ?R4 ?r4)
	(bind ?R5 ?r5)
	(bind ?R6 ?r6)
	(bind ?R7 1)
	(bind ?R8 ?r8)
	(bind ?R9 ?r9)
	(bind ?R10 ?r10)
	(bind ?R11 1)
	(bind ?R12 ?r12)
	(bind ?R13 ?r13)
	(bind ?R14 ?r14)
	(bind ?R15 ?r15)
	(bind ?R16 ?r16)
	(bind ?R17 ?r17)
	(bind ?R18 ?r18)
	(bind ?R19 ?r19)
	(bind ?R20 ?r20)
	(bind ?R21 ?r21)
	(bind ?R22 ?r22)
	(bind ?R23 ?r23)
	(bind ?R24 ?r24)
	(while (neq ?ciclo 0)
		(printout t "[ID: 1]" tab "A quale fascia di potenza e' interessato? (ha risposto " ?r1 ")" crlf)
		(printout t "[ID: 3]" tab "Vorrebbe l'auto nuova? (ha risposto " ?r3 ")" crlf)
		(printout t "[ID: 4]" tab "L'auto le serve per spostarsi in citta' o fuori citta', usualmente? (ha risposto " ?r4 ")" crlf)
		(printout t "[ID: 5]" tab "Che tipo di alimentazione preferirebbe per l'auto? (ha risposto " ?r5 ")" crlf)
		(printout t "[ID: 6]" tab "Che tipo di auto vorrebbe? (ha risposto " ?r6 ")" crlf)
		(printout t "[ID: 7]" tab "Ha qualche preferenza in termini di case automobilistiche? (ha risposto 1)" crlf)
		(printout t "[ID: 8]" tab "Indichi quale tra queste preferisce maggiormente (UNA sola risposta ammessa):" crlf)
		(printout t "[ID: 9]" tab "Vuole che il bagagliaio sia capiente? (ha risposto " ?r9 ")" crlf)
		(printout t "[ID: 10]" tab "Vuole un'auto che e' presente da poco sul mercato o l'inverso? (ha risposto " ?r10 ")" crlf)
		(printout t "[ID: 11]" tab "E' interessato ai sistemi di guida assistita? (ha risposto 1)" crlf)
		(printout t "[ID: 12]" tab "In particolare, troverebbe interessanti i sensori di parcheggio? (ha risposto " ?r12 ")" crlf)
		(printout t "[ID: 13]" tab "E per quanto riguarda le telecamere, le vorrebbe? (ha risposto " ?r13 ")" crlf)
		(printout t "[ID: 14]" tab "Quindi, vorrebbe o meno il parcheggio semi-automatico? (ha risposto " ?r14 ")" crlf)
		(printout t "[ID: 15]" tab "Vorrebbe avere il sistema di controllo elettronico della stabilita'? (ha risposto " ?r15 ")" crlf)
		(printout t "[ID: 16]" tab "Vorrebbe avere il sistema di mantenimento della carreggiata? (ha risposto " ?r16 ")" crlf)
		(printout t "[ID: 17]" tab "Vorrebbe avere il sistema di riconoscimento dei pedoni? (ha risposto " ?r17 ")" crlf)
		(printout t "[ID: 18]" tab "Vorrebbe avere il sistema di riconoscimento della fatica? (ha risposto " ?r18 ")" crlf)
		(printout t "[ID: 19]" tab "Tra il cambio manuale e quello automatico quale preferisce? (ha risposto " ?r19 ")" crlf)
		(printout t "[ID: 20]" tab "E' interessato alla trazione anteriore o integrale (nota anche come 4x4)? (ha risposto " ?r20 ")" crlf)
		(printout t "[ID: 21]" tab "Tra cerchi in lega e acciaio, quali preferisce? (ha risposto " ?r21 ")" crlf)
		(printout t "[ID: 22]" tab "Vorrebbe a bordo un climatizzatore manuale o bi-zona? (ha risposto " ?r22 ")" crlf)
		(printout t "[ID: 23]" tab "Le piacerebbe aprire il portabagagli con un movimento del piede sotto il paraurti? (ha risposto " ?r23 ")" crlf)
		(printout t "[ID: 24]" tab "In quale fascia di prezzo si colloca? (ha risposto " ?r24 ")" crlf)
		(bind ?ciclo (read))
		(switch ?ciclo
			(case 1 then
				(printout t "1- Tra gli 80 e i 100cv" crlf)
				(printout t "2- Tra i 100 e i 120cv" crlf)
				(printout t "3- Oltre i 120cv" crlf)
				(bind ?R1 (read))
			)
			(case 3 then
				(printout t "1- Si, la prendo nuova" crlf)
				(printout t "2- No, la prendo usata" crlf)
				(printout t "3- No, la prendo km 0" crlf)
				(bind ?R3 (read))
			)
			(case 4 then
				(printout t "1- Per spostarmi in citta'" crlf)
				(printout t "2- Per spostarmi fuori citta'" crlf)
				(bind ?R4 (read))
			)
			(case 5 then
				(printout t "1- Diesel" crlf)
				(printout t "2- Ibrida" crlf)
				(printout t "3- Full-electric" crlf)
				(bind ?R5 (read))
			)
			(case 6 then
				(printout t "1- Berlina" crlf)
				(printout t "2- SUV" crlf)
				(printout t "3- Sportiva" crlf)
				(printout t "4- Station-wagon" crlf)
				(printout t "5- Compatta" crlf)
				(printout t "6- Lusso" crlf)
				(bind ?R6 (read))
			)
			(case 7 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R7 (read))
			)
			(case 8 then
				(printout t "1- Ford" crlf)
				(printout t "2- Mazda" crlf)
				(printout t "3- Opel" crlf)
				(printout t "4- Wolfswagen" crlf)
				(printout t "5- Nissan" crlf)
				(printout t "6- Fiat" crlf)
				(printout t "7- Skoda" crlf)
				(printout t "8- Jaguar" crlf)
				(printout t "9- Maserati" crlf)
				(printout t "10- Audi" crlf)
				(printout t "11- Mercedes-Benz" crlf)
				(printout t "12- BMW" crlf)
				(printout t "13- Chevrolet" crlf)
				(printout t "14- Renault" crlf)
				(printout t "15- Smart" crlf)
				(printout t "16- Dacia" crlf)
				(printout t "17- Sukuzi" crlf)
				(bind ?R8 (read))
			)
			(case 9 then
				(printout t "1- Si" crlf)
				(printout t "2- Non necessariamente" crlf)
				(bind ?R9 (read))
			)
			(case 10 then
				(printout t "1- No, una che e' sul mercato da circa 5 anni" crlf)
				(printout t "2- No, una che e' sul mercato da circa 10 anni" crlf)
				(printout t "3- Si, una che e' sul mercato da un paio di anni" crlf)
				(bind ?R10 (read))
			)
			(case 11 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R11 (read))
			)
			(case 12 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R12 (read))
			)
			(case 13 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R13 (read))
			)
			(case 14 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R14 (read))
			)
			(case 15 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R15 (read))
			)
			(case 16 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R16 (read))
			)
			(case 17 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R17 (read))
			)
			(case 18 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R18 (read))
			)
			(case 19 then
				(printout t "1- Manuale" crlf)
				(printout t "2- Automatico" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R19 (read))
			)
			(case 20 then
				(printout t "1- Anteriore" crlf)
				(printout t "2- Integrale" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R20 (read))
			)
			(case 21 then
				(printout t "1- Lega" crlf)
				(printout t "2- Acciaio" crlf)
				(bind ?R21 (read))
			)
			(case 22 then
				(printout t "1- Manuale" crlf)
				(printout t "2- Automatico" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R22 (read))
			)
			(case 23 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R23 (read))
			)
			(case 24 then
				(printout t "1- Non oltre i 30mila euro" crlf)
				(printout t "2- Tra i 30mila e i 45mila euro" crlf)
				(printout t "3- Tra i 45mila e i 60mila euro" crlf)
				(printout t "4- Tra i 60mila e i 75mila euro" crlf)
				(printout t "5- Oltre i 75mila euro" crlf)
				(bind ?R24 (read))
			)
			(default none)
		)
	)
	(retract ?d1)
	(retract ?d3)
	(retract ?d4)
	(retract ?d5)
	(retract ?d6)
	(retract ?d7)
	(retract ?d8)
	(retract ?d9)
	(retract ?d10)
	(retract ?d11)
	(retract ?d12)
	(retract ?d13)
	(retract ?d14)
	(retract ?d15)
	(retract ?d16)
	(retract ?d17)
	(retract ?d18)
	(retract ?d19)
	(retract ?d20)
	(retract ?d21)
	(retract ?d22)
	(retract ?d23)
	(retract ?d24)
	(assert (domande (domanda 1) (risposta ?R1)))
	(assert (domande (domanda 3) (risposta ?R3)))
	(assert (domande (domanda 4) (risposta ?R4)))
	(assert (domande (domanda 5) (risposta ?R5)))
	(assert (domande (domanda 6) (risposta ?R6)))
	(assert (domande (domanda 7) (risposta ?R7)))
	(assert (domande (domanda 8) (risposta ?R8)))
	(assert (domande (domanda 9) (risposta ?R9)))
	(assert (domande (domanda 10) (risposta ?R10)))
	(assert (domande (domanda 11) (risposta ?R11)))
	(assert (domande (domanda 12) (risposta ?R12)))
	(assert (domande (domanda 13) (risposta ?R13)))
	(assert (domande (domanda 14) (risposta ?R14)))
	(assert (domande (domanda 15) (risposta ?R15)))
	(assert (domande (domanda 16) (risposta ?R16)))
	(assert (domande (domanda 17) (risposta ?R17)))
	(assert (domande (domanda 18) (risposta ?R18)))
	(assert (domande (domanda 19) (risposta ?R19)))
	(assert (domande (domanda 20) (risposta ?R20)))
	(assert (domande (domanda 21) (risposta ?R21)))
	(assert (domande (domanda 22) (risposta ?R22)))
	(assert (domande (domanda 23) (risposta ?R23)))
	(assert (domande (domanda 24) (risposta ?R24)))
	(retract ?r)
	(printout t "Il sistema sta elaborando le sue scelte..." crlf)
)

(defrule ritrattazione3
	(declare (salience 20))
	?r <- (ritrattazione (input s))
	(empty (auto ?n1) (accessori ?n2))
	?d1 <- (domande (domanda 1) (risposta ?r1))
	?d3 <- (domande (domanda 3) (risposta ?r3))
	?d4 <- (domande (domanda 4) (risposta ?r4))
	?d5 <- (domande (domanda 5) (risposta ?r5))
	?d6 <- (domande (domanda 6) (risposta ?r6))
	?d7 <- (domande (domanda 7) (risposta 2))
	?d9 <- (domande (domanda 9) (risposta ?r9))
	?d10 <- (domande (domanda 10) (risposta ?r10))
	?d11 <- (domande (domanda 11) (risposta 2))
	?d19 <- (domande (domanda 19) (risposta ?r19))
	?d20 <- (domande (domanda 20) (risposta ?r20))
	?d21 <- (domande (domanda 21) (risposta ?r21))
	?d22 <- (domande (domanda 22) (risposta ?r22))
	?d23 <- (domande (domanda 23) (risposta ?r23))
	?d24 <- (domande (domanda 24) (risposta ?r24))
	=>
	(printout t "Ha appena dato il via alla fase di ritrattazione, in cui puo' modificare le risposte che ha dato alle precedenti domande.
		Puo' modificare la risposta per ciascuna delle domande che seguono attraverso l'inserimento da tastiera del rispettivo ID; prema 0 per terminare la fase di ritrattazione." crlf)
	(if (eq ?n1 0) then
		(if (eq ?n2 0) then
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' non ci sono auto con le caratteristiche tecniche che cerca, nonche' gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte ad alcune domande del genere." crlf)
		else
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' non ci sono auto con le caratteristiche tecniche che cerca, ma esistono linee di auto con gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte alle domande circa le caratteristiche tecniche dell'auto (da ID 1 a ID 10)." crlf)
		)
	else
		(if (eq ?n2 1) then
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' sebbene ci siano auto con le caratteristiche tecniche che cerca, non esistono linee di auto con gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte alle domande circa gli accessori e i componenti personalizzabili (da ID 11 a ID 24)." crlf)
		)
	)
	(bind ?ciclo -1)
	(bind ?R1 ?r1)
	(bind ?R3 ?r3)
	(bind ?R4 ?r4)
	(bind ?R5 ?r5)
	(bind ?R6 ?r6)
	(bind ?R7 2)
	(bind ?R9 ?r9)
	(bind ?R10 ?r10)
	(bind ?R11 2)
	(bind ?R19 ?r19)
	(bind ?R20 ?r20)
	(bind ?R21 ?r21)
	(bind ?R22 ?r22)
	(bind ?R23 ?r23)
	(bind ?R24 ?r24)
	(while (neq ?ciclo 0)
		(printout t "[ID: 1]" tab "A quale fascia di potenza e' interessato? (ha risposto " ?r1 ")" crlf)
		(printout t "[ID: 3]" tab "Vorrebbe l'auto nuova? (ha risposto " ?r3 ")" crlf)
		(printout t "[ID: 4]" tab "L'auto le serve per spostarsi in citta' o fuori citta', usualmente? (ha risposto " ?r4 ")" crlf)
		(printout t "[ID: 5]" tab "Che tipo di alimentazione preferirebbe per l'auto? (ha risposto " ?r5 ")" crlf)
		(printout t "[ID: 6]" tab "Che tipo di auto vorrebbe? (ha risposto " ?r6 ")" crlf)
		(printout t "[ID: 7]" tab "Ha qualche preferenza in termini di case automobilistiche? (ha risposto 2)" crlf)
		(printout t "[ID: 9]" tab "Vuole che il bagagliaio sia capiente? (ha risposto " ?r9 ")" crlf)
		(printout t "[ID: 10]" tab "Vuole un'auto che e' presente da poco sul mercato o l'inverso? (ha risposto " ?r10 ")" crlf)
		(printout t "[ID: 11]" tab "E' interessato ai sistemi di guida assistita? (ha risposto 2)" crlf)
		(printout t "[ID: 19]" tab "Tra il cambio manuale e quello automatico quale preferisce? (ha risposto " ?r19 ")" crlf)
		(printout t "[ID: 20]" tab "E' interessato alla trazione anteriore o integrale (nota anche come 4x4)? (ha risposto " ?r20 ")" crlf)
		(printout t "[ID: 21]" tab "Tra cerchi in lega e acciaio, quali preferisce? (ha risposto " ?r21 ")" crlf)
		(printout t "[ID: 22]" tab "Vorrebbe a bordo un climatizzatore manuale o bi-zona? (ha risposto " ?r22 ")" crlf)
		(printout t "[ID: 23]" tab "Le piacerebbe aprire il portabagagli con un movimento del piede sotto il paraurti? (ha risposto " ?r23 ")" crlf)
		(printout t "[ID: 24]" tab "In quale fascia di prezzo si colloca? (ha risposto " ?r24 ")" crlf)
		(bind ?ciclo (read))
		(switch ?ciclo
			(case 1 then
				(printout t "1- Tra gli 80 e i 100cv" crlf)
				(printout t "2- Tra i 100 e i 120cv" crlf)
				(printout t "3- Oltre i 120cv" crlf)
				(bind ?R1 (read))
			)
			(case 3 then
				(printout t "1- Si, la prendo nuova" crlf)
				(printout t "2- No, la prendo usata" crlf)
				(printout t "3- No, la prendo km 0" crlf)
				(bind ?R3 (read))
			)
			(case 4 then
				(printout t "1- Per spostarmi in citta'" crlf)
				(printout t "2- Per spostarmi fuori citta'" crlf)
				(bind ?R4 (read))
			)
			(case 5 then
				(printout t "1- Diesel" crlf)
				(printout t "2- Ibrida" crlf)
				(printout t "3- Full-electric" crlf)
				(bind ?R5 (read))
			)
			(case 6 then
				(printout t "1- Berlina" crlf)
				(printout t "2- SUV" crlf)
				(printout t "3- Sportiva" crlf)
				(printout t "4- Station-wagon" crlf)
				(printout t "5- Compatta" crlf)
				(printout t "6- Lusso" crlf)
				(bind ?R6 (read))
			)
			(case 7 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R7 (read))
			)
			(case 9 then
				(printout t "1- Si" crlf)
				(printout t "2- Non necessariamente" crlf)
				(bind ?R9 (read))
			)
			(case 10 then
				(printout t "1- No, una che e' sul mercato da circa 5 anni" crlf)
				(printout t "2- No, una che e' sul mercato da circa 10 anni" crlf)
				(printout t "3- Si, una che e' sul mercato da un paio di anni" crlf)
				(bind ?R10 (read))
			)
			(case 11 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R11 (read))
			)
			(case 19 then
				(printout t "1- Manuale" crlf)
				(printout t "2- Automatico" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R19 (read))
			)
			(case 20 then
				(printout t "1- Anteriore" crlf)
				(printout t "2- Integrale" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R20 (read))
			)
			(case 21 then
				(printout t "1- Lega" crlf)
				(printout t "2- Acciaio" crlf)
				(bind ?R21 (read))
			)
			(case 22 then
				(printout t "1- Manuale" crlf)
				(printout t "2- Automatico" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R22 (read))
			)
			(case 23 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R23 (read))
			)
			(case 24 then
				(printout t "1- Non oltre i 30mila euro" crlf)
				(printout t "2- Tra i 30mila e i 45mila euro" crlf)
				(printout t "3- Tra i 45mila e i 60mila euro" crlf)
				(printout t "4- Tra i 60mila e i 75mila euro" crlf)
				(printout t "5- Oltre i 75mila euro" crlf)
				(bind ?R24 (read))
			)
			(default none)
		)
	)
	(retract ?d1)
	(retract ?d3)
	(retract ?d4)
	(retract ?d5)
	(retract ?d6)
	(retract ?d7)
	(retract ?d9)
	(retract ?d10)
	(retract ?d11)
	(retract ?d19)
	(retract ?d20)
	(retract ?d21)
	(retract ?d22)
	(retract ?d23)
	(retract ?d24)
	(assert (domande (domanda 1) (risposta ?R1)))
	(assert (domande (domanda 3) (risposta ?R3)))
	(assert (domande (domanda 4) (risposta ?R4)))
	(assert (domande (domanda 5) (risposta ?R5)))
	(assert (domande (domanda 6) (risposta ?R6)))
	(assert (domande (domanda 7) (risposta ?R7)))
	(assert (domande (domanda 9) (risposta ?R9)))
	(assert (domande (domanda 10) (risposta ?R10)))
	(assert (domande (domanda 11) (risposta ?R11)))
	(assert (domande (domanda 19) (risposta ?R19)))
	(assert (domande (domanda 20) (risposta ?R20)))
	(assert (domande (domanda 21) (risposta ?R21)))
	(assert (domande (domanda 22) (risposta ?R22)))
	(assert (domande (domanda 23) (risposta ?R23)))
	(assert (domande (domanda 24) (risposta ?R24)))
	(retract ?r)
	(printout t "Il sistema sta elaborando le sue scelte..." crlf)
)

(defrule ritrattazione4
	(declare (salience 20))
	?r <- (ritrattazione (input s))
	(empty (auto ?n1) (accessori ?n2))
	?d1 <- (domande (domanda 1) (risposta ?r1))
	?d3 <- (domande (domanda 3) (risposta ?r3))
	?d4 <- (domande (domanda 4) (risposta ?r4))
	?d5 <- (domande (domanda 5) (risposta ?r5))
	?d6 <- (domande (domanda 6) (risposta ?r6))
	?d7 <- (domande (domanda 7) (risposta 1))
	?d8 <- (domande (domanda 8) (risposta ?r8))
	?d9 <- (domande (domanda 9) (risposta ?r9))
	?d10 <- (domande (domanda 10) (risposta ?r10))
	?d11 <- (domande (domanda 11) (risposta 2))
	?d19 <- (domande (domanda 19) (risposta ?r19))
	?d20 <- (domande (domanda 20) (risposta ?r20))
	?d21 <- (domande (domanda 21) (risposta ?r21))
	?d22 <- (domande (domanda 22) (risposta ?r22))
	?d23 <- (domande (domanda 23) (risposta ?r23))
	?d24 <- (domande (domanda 24) (risposta ?r24))
	=>
	(printout t "Ha appena dato il via alla fase di ritrattazione, in cui puo' modificare le risposte che ha dato alle precedenti domande.
		Puo' modificare la risposta per ciascuna delle domande che seguono attraverso l'inserimento da tastiera del rispettivo ID; prema 0 per terminare la fase di ritrattazione." crlf)
	(if (eq ?n1 0) then
		(if (eq ?n2 0) then
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' non ci sono auto con le caratteristiche tecniche che cerca, nonche' gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte ad alcune domande del genere." crlf)
		else
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' non ci sono auto con le caratteristiche tecniche che cerca, ma esistono linee di auto con gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte alle domande circa le caratteristiche tecniche dell'auto (da ID 1 a ID 10)." crlf)
		)
	else
		(if (eq ?n2 1) then
			(printout t "Il sistema ha rilevato che non e' stato restituito alcun risultato perche' sebbene ci siano auto con le caratteristiche tecniche che cerca, non esistono linee di auto con gli accessori che vorrebbe.
				Provi pertanto a rivedere le risposte alle domande circa gli accessori e i componenti personalizzabili (da ID 11 a ID 24)." crlf)
		)
	)
	(bind ?ciclo -1)
	(bind ?R1 ?r1)
	(bind ?R3 ?r3)
	(bind ?R4 ?r4)
	(bind ?R5 ?r5)
	(bind ?R6 ?r6)
	(bind ?R7 1)
	(bind ?R8 ?r8)
	(bind ?R9 ?r9)
	(bind ?R10 ?r10)
	(bind ?R11 1)
	(bind ?R19 ?r19)
	(bind ?R20 ?r20)
	(bind ?R21 ?r21)
	(bind ?R22 ?r22)
	(bind ?R23 ?r23)
	(bind ?R24 ?r24)
	(while (neq ?ciclo 0)
		(printout t "[ID: 1]" tab "A quale fascia di potenza e' interessato? (ha risposto " ?r1 ")" crlf)
		(printout t "[ID: 3]" tab "Vorrebbe l'auto nuova? (ha risposto " ?r3 ")" crlf)
		(printout t "[ID: 4]" tab "L'auto le serve per spostarsi in citta' o fuori citta', usualmente? (ha risposto " ?r4 ")" crlf)
		(printout t "[ID: 5]" tab "Che tipo di alimentazione preferirebbe per l'auto? (ha risposto " ?r5 ")" crlf)
		(printout t "[ID: 6]" tab "Che tipo di auto vorrebbe? (ha risposto " ?r6 ")" crlf)
		(printout t "[ID: 7]" tab "Ha qualche preferenza in termini di case automobilistiche? (ha risposto 1)" crlf)
		(printout t "[ID: 8]" tab "Indichi quale tra queste preferisce maggiormente (UNA sola risposta ammessa):" crlf)
		(printout t "[ID: 9]" tab "Vuole che il bagagliaio sia capiente? (ha risposto " ?r9 ")" crlf)
		(printout t "[ID: 10]" tab "Vuole un'auto che e' presente da poco sul mercato o l'inverso? (ha risposto " ?r10 ")" crlf)
		(printout t "[ID: 11]" tab "E' interessato ai sistemi di guida assistita? (ha risposto 1)" crlf)
		(printout t "[ID: 19]" tab "Tra il cambio manuale e quello automatico quale preferisce? (ha risposto " ?r19 ")" crlf)
		(printout t "[ID: 20]" tab "E' interessato alla trazione anteriore o integrale (nota anche come 4x4)? (ha risposto " ?r20 ")" crlf)
		(printout t "[ID: 21]" tab "Tra cerchi in lega e acciaio, quali preferisce? (ha risposto " ?r21 ")" crlf)
		(printout t "[ID: 22]" tab "Vorrebbe a bordo un climatizzatore manuale o bi-zona? (ha risposto " ?r22 ")" crlf)
		(printout t "[ID: 23]" tab "Le piacerebbe aprire il portabagagli con un movimento del piede sotto il paraurti? (ha risposto " ?r23 ")" crlf)
		(printout t "[ID: 24]" tab "In quale fascia di prezzo si colloca? (ha risposto " ?r24 ")" crlf)
		(bind ?ciclo (read))
		(switch ?ciclo
			(case 1 then
				(printout t "1- Tra gli 80 e i 100cv" crlf)
				(printout t "2- Tra i 100 e i 120cv" crlf)
				(printout t "3- Oltre i 120cv" crlf)
				(bind ?R1 (read))
			)
			(case 3 then
				(printout t "1- Si, la prendo nuova" crlf)
				(printout t "2- No, la prendo usata" crlf)
				(printout t "3- No, la prendo km 0" crlf)
				(bind ?R3 (read))
			)
			(case 4 then
				(printout t "1- Per spostarmi in citta'" crlf)
				(printout t "2- Per spostarmi fuori citta'" crlf)
				(bind ?R4 (read))
			)
			(case 5 then
				(printout t "1- Diesel" crlf)
				(printout t "2- Ibrida" crlf)
				(printout t "3- Full-electric" crlf)
				(bind ?R5 (read))
			)
			(case 6 then
				(printout t "1- Berlina" crlf)
				(printout t "2- SUV" crlf)
				(printout t "3- Sportiva" crlf)
				(printout t "4- Station-wagon" crlf)
				(printout t "5- Compatta" crlf)
				(printout t "6- Lusso" crlf)
				(bind ?R6 (read))
			)
			(case 7 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R7 (read))
			)
			(case 8 then
				(printout t "1- Ford" crlf)
				(printout t "2- Mazda" crlf)
				(printout t "3- Opel" crlf)
				(printout t "4- Wolfswagen" crlf)
				(printout t "5- Nissan" crlf)
				(printout t "6- Fiat" crlf)
				(printout t "7- Skoda" crlf)
				(printout t "8- Jaguar" crlf)
				(printout t "9- Maserati" crlf)
				(printout t "10- Audi" crlf)
				(printout t "11- Mercedes-Benz" crlf)
				(printout t "12- BMW" crlf)
				(printout t "13- Chevrolet" crlf)
				(printout t "14- Renault" crlf)
				(printout t "15- Smart" crlf)
				(printout t "16- Dacia" crlf)
				(printout t "17- Sukuzi" crlf)
				(bind ?R8 (read))
			)
			(case 9 then
				(printout t "1- Si" crlf)
				(printout t "2- Non necessariamente" crlf)
				(bind ?R9 (read))
			)
			(case 10 then
				(printout t "1- No, una che e' sul mercato da circa 5 anni" crlf)
				(printout t "2- No, una che e' sul mercato da circa 10 anni" crlf)
				(printout t "3- Si, una che e' sul mercato da un paio di anni" crlf)
				(bind ?R10 (read))
			)
			(case 11 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(bind ?R11 (read))
			)
			(case 19 then
				(printout t "1- Manuale" crlf)
				(printout t "2- Automatico" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R19 (read))
			)
			(case 20 then
				(printout t "1- Anteriore" crlf)
				(printout t "2- Integrale" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R20 (read))
			)
			(case 21 then
				(printout t "1- Lega" crlf)
				(printout t "2- Acciaio" crlf)
				(bind ?R21 (read))
			)
			(case 22 then
				(printout t "1- Manuale" crlf)
				(printout t "2- Automatico" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R22 (read))
			)
			(case 23 then
				(printout t "1- Si" crlf)
				(printout t "2- No" crlf)
				(printout t "3- Indifferente" crlf)
				(bind ?R23 (read))
			)
			(case 24 then
				(printout t "1- Non oltre i 30mila euro" crlf)
				(printout t "2- Tra i 30mila e i 45mila euro" crlf)
				(printout t "3- Tra i 45mila e i 60mila euro" crlf)
				(printout t "4- Tra i 60mila e i 75mila euro" crlf)
				(printout t "5- Oltre i 75mila euro" crlf)
				(bind ?R24 (read))
			)
			(default none)
		)
	)
	(retract ?d1)
	(retract ?d3)
	(retract ?d4)
	(retract ?d5)
	(retract ?d6)
	(retract ?d7)
	(retract ?d8)
	(retract ?d9)
	(retract ?d10)
	(retract ?d11)
	(retract ?d19)
	(retract ?d20)
	(retract ?d21)
	(retract ?d22)
	(retract ?d23)
	(retract ?d24)
	(assert (domande (domanda 1) (risposta ?R1)))
	(assert (domande (domanda 3) (risposta ?R3)))
	(assert (domande (domanda 4) (risposta ?R4)))
	(assert (domande (domanda 5) (risposta ?R5)))
	(assert (domande (domanda 6) (risposta ?R6)))
	(assert (domande (domanda 7) (risposta ?R7)))
	(assert (domande (domanda 8) (risposta ?R8)))
	(assert (domande (domanda 9) (risposta ?R9)))
	(assert (domande (domanda 10) (risposta ?R10)))
	(assert (domande (domanda 11) (risposta ?R11)))
	(assert (domande (domanda 19) (risposta ?R19)))
	(assert (domande (domanda 20) (risposta ?R20)))
	(assert (domande (domanda 21) (risposta ?R21)))
	(assert (domande (domanda 22) (risposta ?R22)))
	(assert (domande (domanda 23) (risposta ?R23)))
	(assert (domande (domanda 24) (risposta ?R24)))
	(retract ?r)
	(printout t "Il sistema sta elaborando le sue scelte..." crlf)
)